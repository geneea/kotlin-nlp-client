Kotlin Client for Geneea Interpretor G3 API 

Interpretor is a Natural Language Processing platform which helps the users leverage their text data. Features include

- language detection
- entity recognition
- semantic tagging
- sentiment analysis

This package contains Kotlin SDK which enables easy integration of Interpretor General NLP REST API with your code.

For detailed information, please read the [docs](https://help.geneea.com).