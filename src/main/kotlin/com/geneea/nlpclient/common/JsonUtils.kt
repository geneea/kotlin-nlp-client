package com.geneea.nlpclient.common

import com.beust.klaxon.JsonObject

/** Creates a JsonObject from a list of pairs, skipping null values and empty collections/maps. */
public fun json(vararg pairs: Pair<String, Any?>): JsonObject {
    val map = LinkedHashMap<String, Any?>(pairs.size)

    for ((key, value) in pairs) {
        if (value != null) {
            when (value) {
                is Collection<*> -> if (value.isNotEmpty()) map[key] = value.toList()
                is Map<*,*> -> if (value.isNotEmpty()) map[key] = value.toMap()
                else -> map[key] = value
            }
        }
    }

    return JsonObject(map)
}

/** Read an id from a json object */
public fun JsonObject.id() = this.reqString("id")

/** Read an array from a json object */
public inline fun <R> JsonObject.array(key: String, transform: (JsonObject) -> R): List<R>? =
    this.array<JsonObject>(key)?.map { transform(it) }

/**
 * Reads an array of values associated with a [key] from a receiver json object.
 * @throws kotlin.TypeCastException if the value associated with [key] is not a array or is null
 */
public fun <R> JsonObject.reqArray(key: String) = this.array<R>(key)?.toList() as List<R>

/**
 * Reads a string value associated with a [key] from a receiver json object.
 * @throws kotlin.TypeCastException if the value associated with [key] is not a string or is null
 */
public fun JsonObject.reqString(key: String) = this.string(key) as String

/**
 * Reads a string value associated with a [key] from a receiver json object.
 * @throws kotlin.TypeCastException if the value associated with [key] is not a string or is null
 */
public fun JsonObject.reqInt(key: String) = this.int(key) as Int

/**
 * Reads a double value associated with a [key] from a receiver json object.
 * @throws kotlin.TypeCastException if the value associated with [key] is not a number or is null
 */
public fun JsonObject.reqDouble(key: String) = (get(key) as Number).toDouble()
