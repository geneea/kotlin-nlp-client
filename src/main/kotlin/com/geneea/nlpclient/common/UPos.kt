package com.geneea.nlpclient.common

/**
 * Universal POS tags.
 * See http://universaldependencies.org/u/pos/all.html
 */
public enum class UPos {
    // === Open class ===
    /**  adjective  */
    ADJ,
    /**  adverb  */
    ADV,
    /**  interjection  */
    INTJ,
    /**  noun  */
    NOUN,
    /**  proper noun  */
    PROPN,
    /**  verb  */
    VERB,

    // === Closed class ===
    /**  adposition (preposition or postposition) */
    ADP,
    /**  auxiliary  */
    AUX,
    /**  coordinating conjunction  */
    CCONJ,
    /**  determiner  */
    DET,
    /**  numeral  */
    NUM,
    /**  pronoun  */
    PRON,
    /**  particle  */
    PART,
    /**  subordinating conjunction  */
    SCONJ,

    /**  punctuation  */
    PUNCT,
    /**  symbol  */
    SYM,
    /**  other */
    X;

    /**
     * Conversion to the string value used by the API (the string is actually identical).
     */
    public override fun toString(): String = name

    companion object {
        /**
         * Conversion from a [string][posStr]; with [X] as a fallback for unknown values.
         */
        public fun parse(posStr: String): UPos {
            return try {
                valueOf(posStr.toUpperCase())
            } catch (e: IllegalArgumentException) {
                X
            }
        }
    }
}
