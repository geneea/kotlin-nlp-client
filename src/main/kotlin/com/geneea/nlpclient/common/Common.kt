package com.geneea.nlpclient.common

/**
 * Checks whether the receiving iterable is a subsequence of integers, i.e. n, n+1, n+2, ...;
 * true for singletons and empty lists
 */
public fun Iterable<Int>.isSequential() = this.zipWithNext().all { it.first + 1 == it.second }

/**
 * Converts a string to a boolean. "true" and "1" is considered to be true,
 * regardless of case and whitespace padding; all other strings and null are considered to be false.
 */
public fun String?.toBool() = this?.trim()?.toLowerCase()?.let {it == "true" || it == "1" } ?: false
