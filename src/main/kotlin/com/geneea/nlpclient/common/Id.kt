package com.geneea.nlpclient.common

/** Interface of items with an id value. */
public interface HasId {
    val id: String
}

/** Object with an Id */
public abstract class AbstractHasId(override val id: String) : HasId {

    public override fun hashCode(): Int {
        return id.hashCode()
    }

    public override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AbstractHasId

        return (id == other.id)
    }
}

/**
 * Returns a list ids of the receiving list of objects.
 */
public fun <T : HasId> List<T>.ids() = this.map{ it.id }
