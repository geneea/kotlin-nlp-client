package com.geneea.nlpclient.common

/**
 * A tool for [converting][convert] a sorted series of code-point offsets in a [text] to JVM character offsets.
 *
 * For a text of length n and k offsets, the complexity is O(n), while repeated calls to [String.offsetByCodePoints]
 * have complexity O(nk).
 */
public class Cp2JvmOffsetConverter(
    val text: String
) {
    private var lastCharOffset: Int = 0
    private var lastCodepointOffset: Int = 0

    /**
     * Converts a codepoint offset in [text] to a JVM-char-based offset.
     *
     * @throws IndexOutOfBoundsException if the [codepointOffset] is out of bounds of [text],
     *         or when its value in this call is smaller than in the previous call.
     */
    public fun convert(codepointOffset: Int): Int {
        if (codepointOffset < lastCodepointOffset) throw IndexOutOfBoundsException()
        lastCharOffset = text.offsetByCodePoints(lastCharOffset, codepointOffset - lastCodepointOffset)
        lastCodepointOffset = codepointOffset
        return lastCharOffset
    }
}

/**
 * A tool for [converting][convert] a sorted series of JVM-character offsets in a [text] to code-point offsets.
 *
 * For a text of length n and k offsets, the complexity is O(n), while repeated calls to [String.codePointCount]
 * have complexity O(nk).
 */
public class Jvm2CpOffsetConverter(
    val text: String
) {
    private var lastCodepointOffset: Int = 0
    private var lastCharOffset: Int = 0

    /**
     * Converts a JVM-char-based offset in [text] to a codepoint offset.
     *
     * @throws IndexOutOfBoundsException if the [charOffset] is out of bounds of [text],
     *         or when its value in this call is smaller than in the previous call.
     */
    public fun convert(charOffset: Int): Int {
        if (charOffset < lastCharOffset) throw IndexOutOfBoundsException()
        lastCodepointOffset += text.codePointCount(lastCharOffset, charOffset)
        lastCharOffset = charOffset
        return lastCodepointOffset
    }
}
