package com.geneea.nlpclient.common

/** Continuous non-empty span of text, relative to some large text  */
public data class CharSpan internal constructor(
    /** The first character of this span as a zero-based offset within the full text. */
    val start: Int,
    /** Zero-based index of the character immediately following this span. The span cannot be empty. */
    val end: Int
) {

    companion object {
        /**
         * Creates a CharSpan object from [start] index and [end] index.
         * @param start: the first character of this span as a zero-based offset within the full text
         * @param end: index of the character immediately following this span
         */
        public fun of(start: Int, end: Int): CharSpan {
            require(start >= 0) { "Start character index cannot be negative ($start)" }
            require(start < end) { "End character ($end) must be after the start character ($start)" }
            return CharSpan(start, end)
        }

        /**
         * Creates a CharSpan object from [start] index and text [length].
         * @param start: the first character of this span as a zero-based offset within the full text
         * @param length: the length of this span
         */
        public fun withLen(start: Int, length: Int): CharSpan {
            require(start >= 0) { "Start character index cannot be negative ($start)" }
            require(length > 0) { "Length has to be greater than zero ($length)" }
            return CharSpan(start, start + length)
        }
    }

    /** Length of the span in characters  */
    public val length: Int = end - start

    /** Substring of a full text as denoted by this span  */
    public fun extractText(fullText: String): String {
        require(end <= fullText.length) { "Text too short (${fullText.length}) for the span ($this)." }
        return fullText.substring(start, end)
    }
}
