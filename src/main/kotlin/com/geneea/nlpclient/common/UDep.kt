package com.geneea.nlpclient.common


/**
 * Universal syntactic dependencies V2 and V1.
 * See http://universaldependencies.org/u/dep/all.html
 */
public enum class UDep {
    // === Nominals - Core ===
    /**  nominal subject  */
    NSUBJ,
    /**  object (V2)  */
    OBJ,
    /**  indirect object  */
    IOBJ,

    // === Nominals - Non-core dependents ===
    /**  oblique nominal (V2)  */
    OBL,
    /**  vocative  */
    VOCATIVE,
    /**  expletive  */
    EXPL,
    /**  dislocated elements  */
    DISLOCATED,

    // === Nominals - Nominal dependents ===
    /**  nominal modifier  */
    NMOD,
    /**  appositional modifier  */
    APPOS,
    /**  numeric modifier  */
    NUMMOD,

    // === Clauses - Core ===
    /**  clausal subject  */
    CSUBJ,
    /**  clausal complement  */
    CCOMP,
    /**  open clausal complement  */
    XCOMP,

    // === Clauses - Non-core dependents ===
    ADVCL,
    /**  adverbial clause modifier  */

    // === Clauses - Nominal dependent ===
    ACL,
    /**  clausal modifier of noun (adjectival clause)  */

    // === Modifier words - Non-core dependents
    ADVMOD,
    /**  adverbial modifier  */
    DISCOURSE,
    /**  discourse element  */

    // === Modifier words - Nominal dependent ===
    AMOD,
    /**  adjectival modifier  */

    // === Function Words - Non-core dependents ===
    AUX,
    /**  auxiliary  */
    COP,
    /**    */
    MARK,
    /**    */

    // === Function Words - Nominal dependent ===
    /**  determiner  */
    DET,
    /**  classifier (V2)  */
    CLF,
    /**  case marking  */
    CASE,

    // === Coordination ===
    /**  conjunct  */
    CONJ,
    /**  coordinating conjunction  */
    CC,

    // === MWE ===
    /**  fixed multiword expression (V2)  */
    FIXED,
    /**  flat multiword expressio (V2)  */
    FLAT,
    /**  compound  */
    COMPOUND,

    // === Loose ===
    /**  list  */
    LIST,
    /**  parataxis  */
    PARATAXIS,

    // === Special ===
    /**  orphan (V2)  */
    ORPHAN,
    /**  goes with  */
    GOESWITH,
    /**  overridden disfluency  */
    REPARANDUM,

    // === Other ===
    /**  punctuation  */
    PUNCT,
    /**  root  */
    ROOT,
    /**  unspecified dependency  */
    DEP,

    // === V1 ===
    /**  passive auxiliary (V1)  */
    AUXPASS,
    /**  clausal passive subject (V1)  */
    CSUBJPASS,
    /**  direct object (V1; in V2 as obj)  */
    DOBJ,
    /**  foreign words (V1)  */
    FOREIGN,
    /**  multi-word expression (V1)  */
    MWE,
    /**  name (V1)  */
    NAME,
    /**  negation modifier (V1)  */
    NEG,
    /**  passive nominal subject (V1)  */
    NSUBJPASS,
    /**  remnant in ellipsis (V1)  */
    REMNANT;

    /**
     * Conversion to the string value used by the API (the only difference is that the API uses lower case strings).
     */
    public override fun toString(): String = name.toLowerCase()

    companion object {
        /**
         * Conversion from a [string][depStr]; with [DEP] as a fallback for unknown values.
         */
        public fun parse(depStr: String): UDep {
            return try {
                valueOf(depStr.toUpperCase())
            } catch (e: IllegalArgumentException) {
                DEP
            }
        }
    }
}
