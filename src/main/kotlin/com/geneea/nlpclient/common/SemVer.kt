package com.geneea.nlpclient.common

public data class SemVer(
    val major: Int,
    val minor: Int,
    val fix: Int
) : Comparable<SemVer> {

    public override fun compareTo(other: SemVer): Int {
        if (major != other.major) return major - other.major
        if (minor != other.minor) return minor - other.minor
        return fix - other.fix
    }

    companion object {
        private val REGEX = """(\d+)\.(\d+)\.(\d+)""".toRegex()

        public fun valueOf(text: String): SemVer {
            val match = REGEX.matchEntire(text)
            require(match != null) { "Semantic version requires ${REGEX.pattern} format" }
            val (major, minor, fix) = match.destructured
            return SemVer(major.toInt(), minor.toInt(), fix.toInt())
        }
    }
}
