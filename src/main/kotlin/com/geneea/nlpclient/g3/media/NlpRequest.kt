package com.geneea.nlpclient.g3.media

import com.beust.klaxon.JsonObject
import com.geneea.nlpclient.common.array
import com.geneea.nlpclient.common.json
import com.geneea.nlpclient.common.reqString
import com.geneea.nlpclient.g3.client.AnalysisType
import com.geneea.nlpclient.g3.client.ParaSpec
import okhttp3.internal.toImmutableList
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.time.temporal.TemporalAccessor

/** Standard keys used by the G3 request. */
private val STD_KEYS = setOf(
    "id", "title", "lead", "text", "paraSpecs", "domicile", "analyses", "language", "langDetectPrior",
    "presentationLanguage","referenceDate", "categories", "returnMentions", "returnItemSentiment",
    "returnGkbProperties", "metadata"
)

private val REF_DATE_INPUT_FORMAT = DateTimeFormatter.ofPattern("yyyy-M-d")

enum class Taxonomy {
    /** The IPTC Subject Codes taxonomy, see https://iptc.org/standards/subject-codes/ for details. */
    SubjectCode,
    /** The IPTC Media Topics taxonomy, see https://iptc.org/standards/media-topics/ for details. */
    MediaTopic,
    /** Customer specific taxonomy. */
    Custom;

    companion object {
        fun parse(taxonomyStr: String): Taxonomy {
            val lower = taxonomyStr.trim().lowercase()
            return enumValues<Taxonomy>()
                .find { it.name.lowercase() == lower }
                ?: throw IllegalArgumentException("Invalid taxonomy: '${taxonomyStr}'")
        }
    }
}

data class Category internal constructor(
    /** The category taxonomy. */
    val taxonomy: Taxonomy,
    /** The name of the category. */
    val code: String
) {

}

/**
 * An object encapsulating a single REST request for the Media NLP API.
 */
data class NlpRequest internal constructor(
    /** Unique identifier of the document. */
    val id: String?,
    /** The title or subject of the document, when available; mutually exclusive with the ``paraSpecs`` parameter.  */
    val title: String?,
    /** The main text of the document; mutually exclusive with the ``paraSpecs`` parameter. */
    val lead: String?,
    /** The abstract of the document; mutually exclusive with the ``paraSpecs`` parameter. */
    val text: String?,
    /** The document paragraphs; mutually exclusive with `title` and `text` parameters.  */
    val paraSpecs: List<ParaSpec>?,
    /** A domicile indicating the origin location of the document. */
    val domicile: String?,
    /** What analyses to return  */
    val analyses: Set<AnalysisType>?,
    /** The language of the document as ISO 639-1; auto-detection will be used if omitted.  */
    val language: String?,
    /** The language detection prior; e.g. ‘de,en’.  */
    val langDetectPrior: String?,
    /** The presentation language of the results as ISO 639-1; the document language will be used if omitted. */
    val presentationLanguage: String?,
    /** The list of the article categories. */
    val categories: List<Category>?,
    /** Date to be used for the analysis as a reference; values: “NOW” or in format YYYY-MM-DD. */
    val referenceDate: String?,
    /** Should entity/tag/relation mentions be returned? No mentions are returned if omitted. */
    val returnMentions: Boolean?,
    /** Should entity/mention/tag/relation etc. sentiment be returned? No sentiment is returned if omitted. */
    val returnItemSentiment: Boolean?,
    /** Should entity/tag GKB properties be returned? No GKB properties are returned if omitted. */
    val returnGkbProperties: Boolean?,
    /** Extra non-NLP type of information related to the document, key-value pairs. */
    val metadata: Map<String, String>?,
    val customConfig: Map<String, Any?>?
) {

    /**
     * Creates a builder with fields meant to be shared across requests.
     *
     * When analyzing multiple documents, one should:
     *     * first, create a builder specifying all parameters shared by all the analyses to perform
     *       (e.g. [analyses] to perform, [language prior][langDetectPrior]),
     *     * then, use the build function to create individual requests specifying the parameters that are specific
     *       to the analysis of each document (document ID and text of the document, but possibly also `language`, etc).
     */
    class Builder {
        /** What analyses to return. */
        var analyses: Set<AnalysisType>? = null
            private set

        /** The language of the document as ISO 639-1; auto-detection will be used if omitted. */
        var language: String? = null
            private set

        /** The language detection prior; e.g. ‘de,en’. */
        var langDetectPrior: String? = null
            private set

        /** The presentation language of the results as ISO 639-1; the document language will be used if omitted. */
        var presentationLanguage: String? = null
            private set

        /** Date to be used for the analysis as a reference; values: “NOW” or in format YYYY-MM-DD. */
        var referenceDate: String? = null
            private set

        /** Should entity/tag/relation mentions be returned? No mentions are returned if omitted. */
        var returnMentions: Boolean? = null
            private set

        /** Should entity/mention/tag/relation etc. sentiment be returned? No sentiment is returned if omitted. */
        var returnItemSentiment: Boolean? = null
            private set

        /** Should entity/tag GKB properties be returned? No GKB properties are returned if omitted. */
        var returnGkbProperties: Boolean? = null
            private set

        /** Extra non-NLP type of information related to the document, key-value pairs. */
        var metadata: Map<String, String>? = null
            private set

        var customConfig: MutableMap<String, Any?>? = null
            private set

        /** What analyses to return. */
        fun analyses(analyses: Iterable<AnalysisType>) = apply { this.analyses = analyses.toSet() }

        /** What analyses to return. */
        fun analyses(vararg analyses: AnalysisType) = apply { this.analyses = analyses.toSet() }

        /** The language of the document as ISO 639-1; auto-detection will be used if omitted. */
        fun language(language: String) = apply { this.language = language }

        /** The language detection prior; e.g. ‘de,en’. */
        fun langDetectPrior(langDetectPrior: String) = apply { this.langDetectPrior = langDetectPrior }

        /** The presentation language of the results as ISO 639-1; the document language will be used if omitted. */
        fun presentationLanguage(presentationLanguage: String) = apply {
            this.presentationLanguage = presentationLanguage }

        /**
         * Date to be used for the analysis as a reference; either “NOW” or in format YYYY-MM-DD.
         *
         * @throws java.time.format.DateTimeParseException if the date expression has a wrong format
         */
        fun referenceDate(referenceDate: String) = apply {
            this.referenceDate = parseRefDate(referenceDate)
        }

        /**
         * Date and optionally also time and time zone to be used for the analysis as a reference.
         * @param referenceDate The accepted values are [LocalDate], [LocalDateTime], [OffsetDateTime]
         *   or [ZonedDateTime].
         * @throws IllegalArgumentException If [referenceDate] is not one of the expected types.
         */
        fun referenceDate(referenceDate: TemporalAccessor) = apply {
            this.referenceDate = formatRefDate(referenceDate)
        }

        /** Should entity/tag/relation mentions be returned? No mentions are returned if null.  */
        fun returnMentions(returnMentions: Boolean) = apply { this.returnMentions = returnMentions }

        /** Should entity/mention/tag/relation etc. sentiment be returned? No sentiment is returned if null  */
        fun returnItemSentiment(returnItemSentiment: Boolean) = apply {
            this.returnItemSentiment = returnItemSentiment }

        /** Should entity/tag GKB properties be returned? No GKB properties are returned if omitted. */
        fun returnGkbProperties(returnGkbProperties: Boolean) = apply {
            this.returnGkbProperties = returnGkbProperties }

        /** Extra non-NLP type of information related to the document, key-value pairs  */
        fun metadata(metadata: Map<String, String>) = apply { this.metadata = metadata }

        /**
         * Add [custom options][customConfig] to the request builder to be passed to the Media API endpoint.
         * Existing custom options are overwritten.
         */
        fun customConfig(customConfig: Map<String, Any?>) = apply {
            requireNoOverlap(STD_KEYS, customConfig.keys) {
                "Keys ${it.sorted()} overlap with the standard request keys" }
            if (this.customConfig == null) {
                this.customConfig = HashMap(customConfig)
            } else {
                this.customConfig!!.putAll(customConfig)
            }
        }

        private fun formatRefDate(referenceDate: TemporalAccessor) = when (referenceDate) {
            is LocalDate -> DateTimeFormatter.ISO_LOCAL_DATE.format(referenceDate)
            is LocalDateTime, is OffsetDateTime, is ZonedDateTime ->
                DateTimeFormatter.ISO_DATE_TIME.format(referenceDate)
            else -> throw IllegalArgumentException(
                "Unexpected referenceDate: $referenceDate (type: ${referenceDate::class}")
        }

        private fun parseRefDate(referenceDate: String): String =
            if (referenceDate.uppercase() == "NOW") "NOW"
            else {
                val refDate = try {
                    DateTimeFormatter.ISO_DATE_TIME.parseBest(
                        referenceDate, ZonedDateTime::from, OffsetDateTime::from, LocalDateTime::from)
                } catch (e: DateTimeParseException) {
                    LocalDate.parse(referenceDate, REF_DATE_INPUT_FORMAT)
                }
                formatRefDate(refDate)
            }

        /**
         * Creates a new request object to be passed to the G3 client.
         *
         * @param id Unique identifier of the document.
         * @param text The main text of the document; mutually exclusive with the ``paraSpecs`` parameter.
         * @param lead The abstract of the document; mutually exclusive with the ``paraSpecs`` parameter.
         * @param title The title or subject of the document, when available;
         *   mutually exclusive with the ``paraSpecs`` parameter
         * @param paraSpecs The document paragraphs; mutually exclusive with `title`, `lead` and `text` parameters.
         * @param domicile A domicile indicating the origin location of the document.
         * @param language The language of the document as ISO 639-1; auto-detection will be used if omitted.
         * @param presentationLanguage The presentation language of the results as ISO 639-1;
         *   the document language will be used if omitted.
         * @param categories The list of the article categories.
         * @param referenceDate Date to be used for the analysis as a reference; values: ``NOW``
         *   or in format YYYY-MM-DD. No reference date is used if omitted.
         * @param metadata The extra non-NLP type of information related to the document, key-value pairs.
         *
         * @return Request object to be passed to the G3 client.
         */
        @JvmOverloads fun build(
            id: String? = null,
            text: String? = null,
            lead: String? = null,
            title: String? = null,
            paraSpecs: List<ParaSpec>? = null,
            domicile: String? = null,
            language: String? = null,
            presentationLanguage: String? = null,
            categories: List<Category>? = null,  // TODO: use set ?
            referenceDate: String? = null,
            metadata: Map<String, String>? = null,
            customConfig: Map<String, Any?>? = null,
        ): NlpRequest {
            require((text == null && lead == null && title == null) || paraSpecs == null)
            { "Parameters text/lead/title and paraSpecs are mutually exclusive." }
            require(text != null || paraSpecs != null)
            { "Either text or paraSpecs parameter has to be provided." }

            return NlpRequest(
                id = id,
                title = title,
                lead = lead,
                text = text,
                paraSpecs = paraSpecs,
                domicile = domicile,
                analyses = this.analyses,
                language = language ?: this.language,
                langDetectPrior = this.langDetectPrior,
                presentationLanguage = presentationLanguage ?: this.presentationLanguage,
                categories = categories?.toImmutableList(),
                referenceDate = referenceDate?.let { parseRefDate(it) } ?: this.referenceDate,
                returnMentions = this.returnMentions,
                returnItemSentiment = this.returnItemSentiment,
                returnGkbProperties = this.returnGkbProperties,
                metadata = combineMaps(this.metadata, metadata),
                customConfig = combineMaps(this.customConfig, customConfig),
            )
        }
    }

    /** Converts the request object to a Klaxon JSON object. */
    fun toJson(): JsonObject {
        val reqJson = json(
            "id" to id,
            "title" to title,
            "lead" to lead,
            "text" to text,
            "paraSpecs" to paraSpecs?.map { json("type" to it.type, "text" to it.text) },
            "domicile" to domicile,
            "analyses" to analyses?.map { it.name.lowercase() }?.sorted()?.toList(),
            "language" to language,
            "langDetectPrior" to langDetectPrior,
            "presentationLanguage" to presentationLanguage,
            "categories" to categories?.map { json("taxonomy" to it.taxonomy.name, "code" to it.code) },
            "referenceDate" to referenceDate,
            "returnMentions" to if (returnMentions == true) true else null,
            "returnItemSentiment" to if (returnItemSentiment == true) true else null,
            "returnGkbProperties" to if (returnGkbProperties == true) true else null,
            "metadata" to metadata
        )
        if (customConfig != null) reqJson.putAll(customConfig)
        return reqJson
    }

    /** Converts the request object to a JSON string. */
    fun toJsonString(): String = toJson().toJsonString()

    companion object {
        /** Reads a request object from a JSON object. */
        fun fromJson(raw: JsonObject): NlpRequest {
            val title = raw.string("title")
            val lead = raw.string("lead")
            val text = raw.string("text")
            val paraSpecs = raw.array("paraSpecs") {
                ParaSpec(it.reqString("text"), it.reqString("type"))
            }

            require((text == null && lead == null && title == null) || paraSpecs == null)
            { "Parameters text/title and paraSpecs are mutually exclusive." }
            require(text != null || paraSpecs != null)
            { "Either text or paraSpecs parameter has to be provided." }

            val analyses = raw.array<String>("analyses")
                ?.map { AnalysisType.parse(it) }?.toSet()

            val categories = raw.array<JsonObject>("categories")
                ?.map { Category(Taxonomy.parse(it.string("taxonomy")!!), it.string("code")!!) }

            val metadata = raw.obj("metadata")
                ?.mapValues { it.value.toString() }

            val customConfig = raw.keys
                .filter { !STD_KEYS.contains(it) }
                .associateWith { raw[it] }

            return NlpRequest(
                id = raw.string("id"),
                title = title,
                lead = lead,
                text = text,
                paraSpecs = paraSpecs,
                domicile = raw.string("domicile"),
                analyses = analyses,
                language = raw.string("language"),
                langDetectPrior = raw.string("langDetectPrior"),
                presentationLanguage = raw.string("presentationLanguage"),
                categories = categories,
                referenceDate = raw.string("referenceDate"),
                returnMentions = raw.boolean("returnMentions"),
                returnItemSentiment = raw.boolean("returnItemSentiment"),
                returnGkbProperties = raw.boolean("returnGkbProperties"),
                metadata = metadata,
                customConfig = customConfig
            )
        }
    }
}

private fun <K, V> combineMaps(a: Map<K, V>?, b: Map<K, V>?): Map<K, V>? {
    if (a.isNullOrEmpty()) return b
    else if (b.isNullOrEmpty()) return a
    else {
        val c = HashMap<K, V>()
        c.putAll(a)
        c.putAll(b)
        return c
    }
}

private inline fun <V> requireNoOverlap(a: Set<V>?, b: Set<V>?, lazyMessage: (Set<V>) -> Any) {
    val overlap = a.orEmpty() intersect b.orEmpty()
    require(overlap.isEmpty()) { lazyMessage(overlap) }
}
