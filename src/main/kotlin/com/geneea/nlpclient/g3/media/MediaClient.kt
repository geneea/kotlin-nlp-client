package com.geneea.nlpclient.g3.media

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.geneea.nlpclient.g3.io.readFromJson
import com.geneea.nlpclient.g3.model.Analysis
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import java.security.InvalidParameterException
import java.util.concurrent.TimeUnit

/**
 * A client for the Geneea Media NLP API.
 */
class MediaClient internal constructor(
    private val httpClient: OkHttpClient,
    private val jsonParser: Parser,
    private val url: String,
    private val apiKey: String?,
    private val customerId: String?,
) {
    /**
     * Calls the Geneea Media NLP API.

     * @param req The NLP API request object (texts to analyze and parameters).

     * @return result as an Analysis object
    */
    fun analyze(req: NlpRequest): Analysis {
        val httpReq = okhttp3.Request.Builder()
            .url(url)
            .post(req.toJsonString().toRequestBody(JSON_TYPE))
            .header("Accept", JSON_TYPE.toString())
            .also { builder ->
                if (!apiKey.isNullOrBlank()) builder.addHeader("X-API-Key", apiKey)
                if (!customerId.isNullOrBlank()) builder.addHeader("X-Customer-ID", customerId)
            }
            .build()

        httpClient.newCall(httpReq).execute().use { response ->
            if (!response.isSuccessful) {
                val errBody = response.body?.string() ?: "NO CONTENT"
                throw IOException("API call failure with HTTP code ${response.code}\n${errBody}")
            }

            val jsonRes = jsonParser.parse(response.body!!.charStream().buffered()) as JsonObject
            return readFromJson(jsonRes)
        }
    }

    companion object {
        /** The default address of Geneea Media NLP API. */
        const val DEFAULT_URL = "https://media-api.geneea.com/v2/nlp/analyze"
        const val DEFAULT_CONNECT_TIMEOUT_MILLIS = 3050L
        const val DEFAULT_READ_TIMEOUT_SEC = 600L

        private val JSON_TYPE: MediaType = "application/json; charset=utf-8".toMediaType()

        /**
         *  Create Geneea G3 Client.
         *
         *  @param url URL of the Media NLP API to use.
         *  @param apiKey API key; if not specified, loaded from `GENEEA_API_KEY` environment variable.
         *  @param customerId The customer ID; if not specified, loaded from `GENEEA_CUSTOMER_ID` environment variable.
         *  @param connectTimeout Connection timeout in milliseconds; [DEFAULT_CONNECT_TIMEOUT_MILLIS] if not set.
         *  @param readTimeout Read timeout in seconds; [DEFAULT_READ_TIMEOUT_SEC] if not set.
         *  @param additionalSetupBlock Additional configuration of [OkHttpClient] used by the [MediaClient].
         *
         *  @return New instance of Geneea Media API [MediaClient].
         */
        fun create(
            url: String = DEFAULT_URL,
            apiKey: String? = null,
            customerId: String? = null,
            connectTimeout: Long = DEFAULT_CONNECT_TIMEOUT_MILLIS,
            readTimeout: Long = DEFAULT_READ_TIMEOUT_SEC,
            additionalSetupBlock: OkHttpClient.Builder.() -> Unit = {}
        ): MediaClient {
            val httpClient = OkHttpClient().newBuilder()
                .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .also(additionalSetupBlock)
                .build()

            val key = apiKey ?: System.getenv("GENEEA_API_KEY")
            if (key == null) {
                throw InvalidParameterException("No API key specified: neither as parameter " +
                        "nor as GENEEA_API_KEY environment variable.")
            }
            val customer = customerId ?: System.getenv("GENEEA_CUSTOMER_ID")  // it's legit to use no customer ID

            return MediaClient(httpClient, Parser.default(), url, key, customer)
        }
    }
}


