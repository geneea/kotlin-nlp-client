package com.geneea.nlpclient.g3.client

import com.beust.klaxon.JsonObject
import com.geneea.nlpclient.common.array
import com.geneea.nlpclient.common.json
import com.geneea.nlpclient.common.reqString
import com.geneea.nlpclient.g3.model.Paragraph
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * Object specifying the [text] and [type] of a single paragraph.
 */
public data class ParaSpec(
    /**
     * Type of the paragraphs, typically one of [Paragraph.TYPE_TITLE], [Paragraph.TYPE_ABSTRACT], [Paragraph.TYPE_BODY];
     * possibly [Paragraph.TYPE_SECTION_HEADING]
     */
    val type: String,
    /** Text of the paragraph */
    val text: String
) {

    companion object {
        /** Paragraph representing a title of the whole document. It's equivalent to [subject]. */
        public fun title(text: String? = ""): ParaSpec = ParaSpec(Paragraph.TYPE_TITLE, text ?: "")

        /** Paragraph representing a subject of the document or email. It's equivalent to [title]. */
        public fun subject(text: String? = ""): ParaSpec = title(text)

        /** Paragraph representing an abstract of the document. It's equivalent to [lead] and [perex]. */
        public fun abstract(text: String? = ""): ParaSpec = ParaSpec(Paragraph.TYPE_ABSTRACT, text ?: "")

        /** Paragraph representing a lead of the document. It's equivalent to [abstract] and [perex]. */
        public fun lead(text: String? = ""): ParaSpec = abstract(text)

        /** Paragraph representing a perex of the document. It's equivalent to [abstract] and [lead].*/
        public fun perex(text: String? = ""): ParaSpec = abstract(text)

        /** Paragraph containing a body of the document. It's equivalent to [text]. */
        public fun body(text: String? = ""): ParaSpec = ParaSpec(Paragraph.TYPE_BODY, text ?: "")

        /** Paragraph containing a text of the document. It's equivalent to [body]. */
        public fun text(text: String? = ""): ParaSpec = body(text)
    }
}

/**
 * The linguistic analyses the G3 API can perform;
 * [more detail][https://help.geneea.com/api_general/guide/analyses.html]
 */
public enum class AnalysisType {
    /** Perform all analyses at once */
    ALL,
    /** Recognize and standardize entities in text;
     * [more details][https://help.geneea.com/api_general/guide/entities.html]
     */
    ENTITIES,
    /** Assign semantic tags to a document.
     * [more details][<https://help.geneea.com/api_general/guide/tags.html]
     */
    TAGS,
    /** Relations between entities and their attributes;
     * [more details][<https://help.geneea.com/api_general/guide/relations.html]
     */
    RELATIONS,
    /** Detect the emotions of the author contained in the text;
     * [more details][https://help.geneea.com/api_general/guide/sentiment.html]
     */
    SENTIMENT,
    /** Detect the language the text is written in;
     * [more details][https://help.geneea.com/api_general/guide/language.html]
     */
    LANGUAGE;

    companion object {
        public fun parse(typeStr: String): AnalysisType {
            return try {
                valueOf(typeStr.trim().toUpperCase())
            } catch (e: IllegalArgumentException) {
                throw IllegalArgumentException("Invalid analysis type '${typeStr}'")
            }
        }
    }
}

/** Typically used ISO 639-1 language codes. */
public class LanguageCode {
    companion object {
        const val CS = "cs"
        const val DE = "de"
        const val EN = "en"
        const val ES = "es"
        const val PL = "pl"
        const val SK = "sk"
    }
}

/** Typically used domains. For more info [see][https://help.geneea.com/api_general/guide/domains.html] */
public class Domain {
    companion object {
        /** General media articles. */
        public const val MEDIA = "media"
        /** Media articles covering news. */
        public const val NEWS = "news"
        /** Media articles covering sport news. */
        public const val SPORT = "sport"
        /** Tabloid articles. */
        public const val TABLOID = "tabloid"
        /** Media articles covering technology and science. */
        public const val TECH = "tech"
        /** General Voice-of-the customer documents (e.g. reviews). */
        public const val VOC = "voc"
        /** Voice-of-the customer documents covering banking (e.g. reviews of banks). */
        public const val VOC_BANKING = "voc-banking"
        /** Voice-of-the customer documents covering restaurants (e.g. reviews of restaurants). */
        public const val VOC_HOSPITALITY = "voc-hospitality"
    }
}

/** Typically used text types.  */
public class TextType {
    companion object {
        /** Text that is mostly grammatically, orthographically and typographically correct, e.g. news articles.  */
        public const val CLEAN = "clean"
        /** Text that ignores many formal grammatical, orthographical and typographical conventions,
        e.g. social media posts.  */
        public const val CASUAL = "casual"
    }
}

/** Supported diacritization models. */
public class Diacritization {
    companion object {
        /** No diacritization is performed. */
        public const val NONE = "none"
        /** Diacritics is added if needed. */
        public const val AUTO = "auto"
        /** Diacritics is added to words without it if needed. */
        public const val YES = "yes"
        /** Diacritics is first removed and then added if needed. */
        public const val REDO = "redo"
    }
}

/** Standard keys used by the G3 request. */
private val STD_KEYS = setOf(
    "id", "title", "text", "paraSpecs", "analyses", "htmlExtractor", "language", "langDetectPrior", "domain",
    "textType", "referenceDate", "diacritization", "returnMentions", "returnItemSentiment", "metadata"
)

private val REF_DATE_INPUT_FORMAT = DateTimeFormatter.ofPattern("yyyy-M-d")

/**
 * An object encapsulating a single REST request for the G3 API.
 */
public data class Request internal constructor(
    /** Unique identifier of the document */
    val id: String?,
    /** The title or subject of the document, when available; mutually exclusive with the ``paraSpecs`` parameter  */
    val title: String?,
    /** The main text of the document; mutually exclusive with the ``paraSpecs`` parameter */
    val text: String?,
    /** The document paragraphs; mutually exclusive with `title` and `text` parameters.  */
    val paraSpecs: List<ParaSpec>?,
    /** What analyses to return  */
    val analyses: Set<AnalysisType>?,
    /** The language of the document as ISO 639-1; auto-detection will be used if omitted.  */
    val language: String?,
    /** The language detection prior; e.g. ‘de,en’.  */
    val langDetectPrior: String?,
    /** The source domain from which the document originates.
    See the [available domains][https://help.geneea.com/api_general/guide/domains.html]  */
    val domain: String?,
    /** The type or genre of text; not supported in public workflows/domains yet.  */
    val textType: String?,
    /** Date to be used for the analysis as a reference; values: “NOW” or in format YYYY-MM-DD. */
    val referenceDate: String?,
    /** Determines whether to perform text diacritization  */
    val diacritization: String?,
    /** Should entity/tag/relation mentions be returned? No mentions are returned if null.  */
    val returnMentions: Boolean?,
    /** Should entity/mention/tag/relation etc. sentiment be returned? No sentiment is returned if null  */
    val returnItemSentiment: Boolean?,
    /** Extra non-NLP type of information related to the document, key-value pairs  */
    val metadata: Map<String, String>?,
    val customConfig: Map<String, Any?>?
) {

    /**
     * Creates a builder with fields meant to be shared across requests.
     *
     * When analyzing multiple documents, one should:
     *     * first, create a builder specifying all parameters shared by all the analyses to perform (e.g. [analyses] to perform,
     * [language prior][langDetectPrior]),
     *     * then, use the build function to create individual requests specifying the parameters that are specific
     * for the analysis of each document (Id and text of the document, but possibly also `language`, etc).
     */
    public class Builder {
        /** What analyses to return  */
        var analyses: Set<AnalysisType>? = null
            private set

        /** The language of the document as ISO 639-1; auto-detection will be used if omitted.  */
        var language: String? = null
            private set

        /** The language detection prior; e.g. ‘de,en’.  */
        var langDetectPrior: String? = null
            private set

        /** The source domain from which the document originates.
        See the [available domains][https://help.geneea.com/api_general/guide/domains.html]  */
        var domain: String? = null
            private set

        /** The type or genre of text; not supported in public workflows/domains yet.  */
        var textType: String? = null
            private set

        /** Date to be used for the analysis as a reference; values: “NOW” or in format YYYY-MM-DD. */
        var referenceDate: String? = null
            private set

        /** Determines whether to perform text diacritization  */
        var diacritization: String? = null
            private set

        /** Should entity/tag/relation mentions be returned? No mentions are returned if null.  */
        var returnMentions: Boolean? = null
            private set

        /** Should entity/mention/tag/relation etc. sentiment be returned? No sentiment is returned if null  */
        var returnItemSentiment: Boolean? = null
            private set

        /** Extra non-NLP type of information related to the document, key-value pairs  */
        var metadata: Map<String, String>? = null
            private set

        var customConfig: MutableMap<String, Any?>? = null
            private set

        /** What analyses to return  */
        public fun analyses(analyses: Iterable<AnalysisType>) = apply { this.analyses = analyses.toSet() }

        /** What analyses to return  */
        public fun analyses(vararg analyses: AnalysisType) = apply { this.analyses = analyses.toSet() }

        /** The language of the document as ISO 639-1; auto-detection will be used if omitted.  */
        public fun language(language: String) = apply { this.language = language }

        /** The language detection prior; e.g. ‘de,en’.  */
        public fun langDetectPrior(langDetectPrior: String) = apply { this.langDetectPrior = langDetectPrior }

        /** The source domain from which the document originates.
        See the [available domains][https://help.geneea.com/api_general/guide/domains.html]  */
        public fun domain(domain: String) = apply { this.domain = domain }

        /** The type or genre of text; not supported in public workflows/domains yet.  */
        public fun textType(textType: String) = apply { this.textType = textType }

        /**
         * Date to be used for the analysis as a reference; either “NOW” or in format YYYY-MM-DD.
         *
         * @throws java.time.format.DateTimeParseException if the date expression has a wrong format
         */
        public fun referenceDate(referenceDate: String) = apply {
            this.referenceDate = parseRefDate(referenceDate)
        }

        /** Date to be used for the analysis as a reference. */
        public fun referenceDate(referenceDate: LocalDate) = apply {
            this.referenceDate = formatRefDate(referenceDate)
        }

        /** Determines whether to perform text diacritization  */
        public fun diacritization(diacritization: String) = apply { this.diacritization = diacritization }

        /** Should entity/tag/relation mentions be returned? No mentions are returned if null.  */
        public fun returnMentions(returnMentions: Boolean) = apply { this.returnMentions = returnMentions }

        /** Should entity/mention/tag/relation etc. sentiment be returned? No sentiment is returned if null  */
        public fun returnItemSentiment(returnItemSentiment: Boolean) = apply { this.returnItemSentiment = returnItemSentiment }

        /** Extra non-NLP type of information related to the document, key-value pairs  */
        public fun metadata(metadata: Map<String, String>) = apply { this.metadata = metadata }

        /**
         * Add [custom options][customConfig] to the request builder to be passed to the G3 API endpoint.
         * Existing custom options are overwritten.
         */
        public fun customConfig(customConfig: Map<String, Any?>) = apply {
            requireNoOverlap(STD_KEYS, customConfig.keys)
            { "Keys ${it.sorted()} overlap with the standard request keys" }
            if (this.customConfig == null) {
                this.customConfig = HashMap(customConfig)
            } else {
                this.customConfig!!.putAll(customConfig)
            }
        }

        private fun formatRefDate(date: LocalDate) = DateTimeFormatter.ISO_LOCAL_DATE.format(date)

        private fun parseRefDate(referenceDate: String): String =
            if (referenceDate.toUpperCase() == "NOW") "NOW"
            else formatRefDate(LocalDate.parse(referenceDate, REF_DATE_INPUT_FORMAT))

        /**
         * Creates a new request object to be passed to the G3 client.
         *
         * @param id Unique identifier of the document
         * @param text The main text of the document; mutually exclusive with the ``paraSpecs`` parameter
         * @param title The title or subject of the document, when available; mutually exclusive with the ``paraSpecs`` parameter
         * @param paraSpecs The document paragraphs; mutually exclusive with `title` and `text` parameters.
         * @param language The language of the document as ISO 639-1; auto-detection will be used if null.
         * @param referenceDate Date to be used for the analysis as a reference; values: ``NOW`` or in format YYYY-MM-DD.
         *   No reference date is used if null.
         * @param metadata extra non-NLP type of information related to the document, key-value pairs
         * @param customConfig Any custom options passed to the G3 API endpoint
         * @return Request object to be passed to the G3 client.
         */
        @JvmOverloads public fun build(
            id: String? = null,
            text: String? = null,
            title: String? = null,
            paraSpecs: List<ParaSpec>? = null,
            language: String? = null,
            referenceDate: String? = null,
            metadata: Map<String, String>? = null,
            customConfig: Map<String, Any?>? = null
        ): Request {
            require((text == null && title == null) || paraSpecs == null)
            { "Parameters text/title and paraSpecs are mutually exclusive" }
            require(text != null || paraSpecs != null)
            { "Either text or paraSpecs parameter has to be provided" }
            requireNoOverlap(STD_KEYS, customConfig?.keys)
            { "Keys ${it.sorted()} overlap with the standard request keys" }

            return Request(
                id = id,
                title = title,
                text = text,
                paraSpecs = paraSpecs,
                analyses = this.analyses,
                language = language ?: this.language,
                langDetectPrior = this.langDetectPrior,
                domain = this.domain,
                textType = this.textType,
                referenceDate = referenceDate?.let { parseRefDate(it) } ?: this.referenceDate,
                diacritization = this.diacritization,
                returnMentions = this.returnMentions,
                returnItemSentiment = this.returnItemSentiment,
                metadata = combineMaps(this.metadata, metadata),
                customConfig = combineMaps(this.customConfig, customConfig)
            )
        }
    }

    /** Converts the request object to a Klaxon JSON object. */
    public fun toJson(): JsonObject {
        val reqJson = json(
            "id" to id,
            "title" to title,
            "text" to text,
            "paraSpecs" to paraSpecs?.map { json("type" to it.type, "text" to it.text) },
            "analyses" to analyses?.map { it.name.toLowerCase() }?.sorted()?.toList(),
            "language" to language,
            "langDetectPrior" to langDetectPrior,
            "domain" to domain,
            "textType" to textType,
            "referenceDate" to referenceDate,
            "diacritization" to diacritization,
            "returnMentions" to if (returnMentions == true) true else null,
            "returnItemSentiment" to if (returnItemSentiment == true) true else null,
            "metadata" to metadata
        )

        if (customConfig != null) reqJson.putAll(customConfig)

        return reqJson
    }

    /** Converts the request object to a JSON string. */
    public fun toJsonString(): String = toJson().toJsonString()

    companion object {
        /** Reads a request object from a JSON object. */
        public fun fromJson(raw: JsonObject): Request {
            val title = raw.string("title")
            val text = raw.string("text")
            val paraSpecs = raw.array("paraSpecs") {
                ParaSpec(it.reqString("text"), it.reqString("type"))
            }

            require((text == null && title == null) || paraSpecs == null)
            { "Parameters text/title and paraSpecs are mutually exclusive" }
            require(text != null || paraSpecs != null)
            { "Either text or paraSpecs parameter has to be provided" }

            val analyses = raw.array<String>("analyses")
                ?.map { AnalysisType.parse(it) }?.toSet()

            val metadata = raw.obj("metadata")
                ?.mapValues { it.value.toString() }

            val customConfig = raw.keys
                .filter { !STD_KEYS.contains(it) }
                .associateWith { raw[it] }

            return Request(
                id = raw.string("id"),
                title = title,
                text = text,
                paraSpecs = paraSpecs,
                analyses = analyses,
                language = raw.string("language"),
                langDetectPrior = raw.string("langDetectPrior"),
                domain = raw.string("domain"),
                textType = raw.string("textType"),
                referenceDate = raw.string("referenceDate"),
                diacritization = raw.string("diacritization"),
                returnMentions = raw.boolean("returnMentions"),
                returnItemSentiment = raw.boolean("returnItemSentiment"),
                metadata = metadata,
                customConfig = customConfig
            )
        }
    }
}

private fun <K, V> combineMaps(a: Map<K, V>?, b: Map<K, V>?): Map<K, V>? {
    if (a.isNullOrEmpty()) return b
    else if (b.isNullOrEmpty()) return a
    else {
        val c = HashMap<K, V>()
        c.putAll(a)
        c.putAll(b)
        return c
    }
}

private inline fun <V> requireNoOverlap(a: Set<V>?, b: Set<V>?, lazyMessage: (Set<V>) -> Any) {
    val overlap = a.orEmpty() intersect b.orEmpty()
    require(overlap.isEmpty()) { lazyMessage(overlap) }
}
