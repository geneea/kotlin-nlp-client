package com.geneea.nlpclient.g3.client

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.geneea.nlpclient.g3.io.readFromJson
import com.geneea.nlpclient.g3.model.Analysis
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import java.util.concurrent.TimeUnit

/**
 * A client for the Geneea NLP REST (General API V3).
 */
public class Client internal constructor(
    private val httpClient: OkHttpClient,
    private val jsonParser: Parser,
    private val url: String,
    private val userKey: String?
) {
    /**
     * Call Geneea G3 API.
     * @param req request (data to analyze and parameters)
     * @return result as an Analysis object
    */
    public fun analyze(req: Request): Analysis {
        val httpReq = okhttp3.Request.Builder()
            .url(url)
            .post(req.toJsonString().toRequestBody(JSON_TYPE))
            .header("Accept", JSON_TYPE.toString())
            .header("Authorization", "user_key ${userKey ?: "ANONYMOUS"}")
            .build()

        httpClient.newCall(httpReq).execute().use { response ->
            if (!response.isSuccessful) {
                val errBody = response.body?.string() ?: "NO CONTENT"
                throw IOException("API call failure with HTTP code ${response.code}\n${errBody}")
            }

            val jsonRes = jsonParser.parse(response.body!!.charStream().buffered()) as JsonObject
            return readFromJson(jsonRes)
        }
    }

    companion object {
        /** The default address of the Geneea NLP G3 API. */
        public const val DEFAULT_URL = "https://api.geneea.com/v3/analysis"
        public const val DEFAULT_CONNECT_TIMEOUT_MILLIS = 3050L
        public const val DEFAULT_READ_TIMEOUT_SEC = 600L

        private val JSON_TYPE: MediaType = "application/json; charset=utf-8".toMediaType()

        /**
         *  Create Geneea G3 Client.
         *
         *  @param url URL of the Interpretor API to use
         *  @param userKey API user key; if not specified, loaded from `GENEEA_API_KEY` environment variable.
         *  @param connectTimeout connection timeout in milliseconds; [DEFAULT_CONNECT_TIMEOUT_MILLIS] if not set
         *  @param readTimeout read timeout in seconds; [DEFAULT_READ_TIMEOUT_SEC] if not set
         *  @param additionalSetupBlock additional configuration of [OkHttpClient] used by the [Client].
         *  @return G3 client
         */
        public fun create(
            url: String = DEFAULT_URL,
            userKey: String? = null,
            connectTimeout: Long = DEFAULT_CONNECT_TIMEOUT_MILLIS,
            readTimeout: Long = DEFAULT_READ_TIMEOUT_SEC,
            additionalSetupBlock: OkHttpClient.Builder.() -> Unit = {}
        ): Client {
            val httpClient = OkHttpClient().newBuilder()
                .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .also(additionalSetupBlock)
                .build()

            val key = userKey ?: System.getenv("GENEEA_API_KEY")
            if (key == null) {
                println("[Warning] No user key specified: neither as parameter nor as GENEEA_API_KEY environment variable")
            }

            return Client(httpClient, Parser.default(), url, key)
        }
    }
}


