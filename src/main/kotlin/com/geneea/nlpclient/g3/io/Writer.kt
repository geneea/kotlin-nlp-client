package com.geneea.nlpclient.g3.io

import com.beust.klaxon.JsonObject
import com.geneea.nlpclient.common.*
import com.geneea.nlpclient.g3.model.*

/** Writes the Analysis to a JSON object in a format as returned by Geneea G3 API. */
public fun writeToJson(analysis: Analysis): JsonObject = Writer().toJson(analysis)

internal class Writer {
    // NOTE: this impl. is not thread-safe

    private var offMap = OffsetMapping.IDENTITY

    fun toJson(obj: Analysis) = json(
        "version" to "3.3.0",
        "id" to obj.docId,
        "language" to json("detected" to obj.language.detected),
        "paragraphs" to obj.paragraphs.map { toRawPara(it) },
        "entities" to obj.entities.map { toRawEntity(it) },
        "tags" to obj.tags.map { toRawTag(it) },
        "relations" to obj.relations.map { toRawRelation(it) },
        "docSentiment" to obj.docSentiment?.let { toRawSentiment(it) },
        "docVectors" to obj.docVectors?.let { toRawVectors(it) },
        "itemSentiments" to createRawItemSentiment(obj),
        "itemVectors" to createRawItemVectors(obj),
        "usedChars" to obj.usedChars,
        "metadata" to obj.metadata,
        "debugInfo" to obj.debugInfo
    )

    private fun toRawPara(p: Paragraph): JsonObject {
        offMap = OffsetMapping.create(p.text, p.origText)
        return json(
            "id" to p.id,
            "type" to p.type,
            "text" to p.text,
            "origText" to if (p.origText != p.text) p.origText else null,
            "sentences" to p.sentences.map { toRawSentence(it) }
        )
    }

    private fun toRawSentence(s: Sentence) = json(
        "id" to s.id,
        "tokens" to s.tokens.map { toRawToken(it) },
        "tecto" to s.tectoTokens?.map { toRawTectoToken(it) }
    )

    private fun toRawToken(t: Token): JsonObject {
        val off = t.charSpan.start.let { offMap.get(it) }
        val origOff = t.origCharSpan.start.let { offMap.getOrig(it) }
        return json(
            "id" to t.id,
            "off" to off,
            "text" to t.text,
            "origOff" to if (origOff != off) origOff else null,
            "origText" to if (t.origText != t.text) t.origText else null,
            "dLemma" to t.deepLemma,
            "lemma" to t.lemma,
            "pos" to t.pos?.toString(),
            "mTag" to t.morphTag,
            "feats" to t.feats,
            "fnc" to t.fullFnc?.toLowerCase(),
            "parId" to t.parent?.id
        )
    }

    private fun toRawTectoToken(t: TectoToken) = json(
        "id" to t.id,
        "fnc" to t.fnc.toLowerCase(),
        "parId" to t.parent?.id,
        "lemma" to t.lemma,
        "feats" to t.feats,
        "tokenIds" to t.tokens?.ids(),
        "entityMentionId" to t.entityMention?.id
    )

    private fun toRawEntity(e: Entity) = json(
        "id" to e.id,
        "gkbId" to e.gkbId,
        "stdForm" to e.stdForm,
        "type" to e.type,
        "feats" to e.feats,
        "mentions" to e.mentions.map { toRawEntityMention(it) },
        "gkbProperties" to e.gkbProperties.map { toRawGkbProperty(it) }
    )

    private fun toRawEntityMention(m: EntityMention) = json(
        "id" to m.id,
        "text" to m.text,
        "mwl" to m.mwl,
        "tokenIds" to m.tokens.ids(),
        "feats" to m.feats,
        "derivedFromEntityId" to m.derivedFrom?.id
    )

    private fun toRawTag(t: Tag) = json(
        "id" to t.id,
        "gkbId" to t.gkbId,
        "stdForm" to t.stdForm,
        "type" to t.type,
        "relevance" to t.relevance,
        "feats" to t.feats,
        "mentions" to t.mentions.map { toRawTagMention(it) },
        "gkbProperties" to t.gkbProperties.map { toRawGkbProperty(it) }
    )

    private fun toRawTagMention(m: TagMention) = json(
        "id" to m.id,
        "tokenIds" to m.tokens.ids(),
        "feats" to m.feats
    )

    private fun toRawGkbProperty(m: GkbProperty) = json(
        "name" to m.name,
        "label" to m.label,
        "valueGkbId" to m.valueGkbId,
        "boolValue" to m.boolValue,
        "floatValue" to m.floatValue,
        "intValue" to m.intValue,
        "strValue" to m.strValue,
    )

    private fun toRawRelation(r: Relation) = json(
        "id" to r.id,
        "textRepr" to r.textRepr,
        "name" to r.name,
        "type" to r.type,
        "args" to r.args.map { toRawRelationArgument(it) },
        "feats" to r.feats,
        "support" to r.support.map { toRawRelationSupport(it) }
    )

    private fun toRawRelationArgument(a: RelationArgument) = json(
        "name" to a.name,
        "type" to a.type,
        "entityId" to a.entity?.id
    )

    private fun toRawRelationSupport(s: RelationSupport) = json(
        "tokenIds" to s.tokens.ids(),
        "tectoId" to s.tectoToken?.id
    )

    private fun toRawSentiment(s: Sentiment) = json(
        "mean" to s.mean,
        "label" to s.label,
        "positive" to s.positive,
        "negative" to s.negative
    )

    private fun toRawVectors(vectors: List<Vector>) =
        vectors.map { json(
            "name" to it.name,
            "version" to it.version,
            "values" to it.values.toList()
        ) }

    private fun createRawItemSentiment(obj: Analysis): Map<String, JsonObject> {
        val id2sentiment = mutableMapOf<String, JsonObject>()
        fun List<HasSentiment>.registerSentiment() = this.forEach {
            if (it.sentiment != null) {
                id2sentiment[it.id] = toRawSentiment(it.sentiment as Sentiment)
            }
        }

        obj.paragraphs.registerSentiment()
        obj.paragraphs.forEach { it.sentences.registerSentiment() }
        obj.entities.registerSentiment()
        obj.entities.forEach { it.mentions.registerSentiment() }
        obj.tags.registerSentiment()
        obj.tags.forEach { it.mentions.registerSentiment() }
        obj.relations.registerSentiment()

        return id2sentiment
    }

    private fun createRawItemVectors(obj: Analysis): Map<String, List<JsonObject>>  {
        val id2vectors = mutableMapOf<String, List<JsonObject>>()
        fun List<HasVectors>.registerVectors() = this.forEach {
            if (it.vectors != null) {
                id2vectors[it.id] = toRawVectors(it.vectors as List<Vector>)
            }
        }

        obj.paragraphs.registerVectors()
        obj.paragraphs.forEach { it.sentences.registerVectors() }
        obj.entities.registerVectors()
        obj.entities.forEach { it.mentions.registerVectors() }
        obj.tags.registerVectors()
        obj.tags.forEach { it.mentions.registerVectors() }
        obj.relations.registerVectors()

        return id2vectors
    }

    /** Encapsulates mapping of offsets from JVM-char-based to code-points. */
    private interface OffsetMapping {
        fun get(jvmOff: Int): Int
        fun getOrig(jvmOff: Int): Int

        companion object {
            val IDENTITY = object : OffsetMapping {
                override fun get(jvmOff: Int) = jvmOff
                override fun getOrig(jvmOff: Int) = jvmOff
            }

            fun create(text: String, origText: String) : OffsetMapping {
                val convForText = Jvm2CpOffsetConverter(text)
                val convForOrigText = if (origText === text) convForText else Jvm2CpOffsetConverter(origText)

                return object : OffsetMapping {
                    override fun get(jvmOff: Int) = convForText.convert(jvmOff)
                    override fun getOrig(jvmOff: Int) = convForOrigText.convert(jvmOff)
                }
            }
        }
    }
}
