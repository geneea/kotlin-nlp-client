package com.geneea.nlpclient.g3.io

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.geneea.nlpclient.common.*
import com.geneea.nlpclient.g3.model.*

/**
 * Reads the G3 object from a JSON object as returned from Geneea G3 API.
 *
 * @param raw: raw JSON object corresponding to the G3 API
 * @return G3 object encapsulating the analysis
 *
 * Note: depending on requested set of analyses and language support many of the keys can be missing
 */
public fun readFromJson(raw: JsonObject): Analysis = Reader().fromJson(raw)

/** Standard keys used in G3 analysis JSON. */
private val STD_KEYS = setOf(
    "id", "language", "paragraphs",
    "entities", "tags", "relations",
    "docSentiment", "itemSentiments", "docVectors", "itemVectors",
    "usedChars", "metadata", "debugInfo", "version"
)

private val FULL_DEP_FNC_REGEX = """^([a-z]+):([a-z]+)$""".toRegex(RegexOption.IGNORE_CASE)

internal class Reader {
    // NOTE: this impl. is not thread-safe

    /** Registry to store read objects by their ids; used to resolve references */
    private val registry = mutableMapOf<String, HasId>()

    private val id2sentiment = mutableMapOf<String, Sentiment>()
    private val id2vectors = mutableMapOf<String, List<Vector>>()

    private val tectoTokenId2mentionId = mutableMapOf<String, String>()
    private val entityId2derivedMentions = mutableMapOf<String, MutableList<EntityMention>>()

    private var offMap = OffsetMapping.IDENTITY

    /** Version of the JSON being read */
    private var version = SemVer(3, 3, 0)

    private fun register(obj: HasId) {
        registry[obj.id] = obj
    }

    private fun <T : HasId> resolveId(id: String): T {
        val obj = registry[id]
        require(obj != null) { "Unknown object ID used as a reference: '$id'" }
        @Suppress("UNCHECKED_CAST") // it is ok to throw an exception for a corrupted json
        return obj as T
    }

    private fun <T : HasId> readAndResolveId(raw: JsonObject, key: String) =
        raw.string(key)?.let { resolveId<T>(it) }

    private fun <T : HasId> readAndResolveIds(raw: JsonObject, key: String) =
        raw.array<String>(key)?.map { resolveId<T>(it) }

    private fun checkVersion(version: String?) {
        val semVer = SemVer.valueOf(version ?: "3.0.0") // 3.0.0 was not specified
        require(semVer < SemVer(3, 4, 0))
        { "unsupported API version \"$version\", major ver.num != 3 or minor ver.num > 3" }
        this.version = semVer
    }

    private fun readSentiment(raw: JsonObject) =
        Sentiment(
            mean = raw.reqDouble("mean"),
            label = raw.reqString("label"),
            positive = raw.reqDouble("positive"),
            negative = raw.reqDouble("negative")
        )

    private fun readVector(raw: JsonObject) =
        Vector(
            name = raw.reqString("name"),
            version = raw.reqString("version"),
            values = raw.reqArray<Double>("values").toDoubleArray()
        )

    private fun readGkbProperty(raw: JsonObject) = GkbProperty(
            name = raw.reqString("name"),
            label = raw.string("label"),
            valueGkbId = raw.string("valueGkbId"),
            boolValue = raw.boolean("boolValue"),
            intValue = raw.long("intValue"),
            floatValue = raw.double("floatValue"),
            strValue = raw.string("strValue"),
        )

    @Suppress("UNCHECKED_CAST") // it is ok to throw an exception for a corrupted json
    private fun readFeats(raw: JsonObject?) = raw?.let { (it as Map<String, String>).toMap() }

    private fun readToken(raw: JsonObject, tokenIdx: Int, useOrigTextField: Boolean): Token {
        val text: String; val origText: String; val off: Int; val origOff: Int
        if (useOrigTextField) {
            text = raw.reqString("text")
            off = raw.reqInt("off").let { offMap.get(it) }
            origText = raw.string("origText") ?: text
            origOff = raw.int("origOff")?.let { offMap.getOrig(it) } ?: off
        } else {
            text = raw.reqString("corrText")
            off = raw.reqInt("corrOff").let { offMap.get(it) }
            origText = raw.reqString("text")
            origOff = raw.reqInt("off").let { offMap.getOrig(it) }
        }

        val pos: UPos? = raw.string("pos")?.let { UPos.parse(it) }

        var fnc: UDep? = null
        var subFnc: String? = null
        raw.string("fnc")?.also {
            if (it.toUpperCase() == "CLAUSE") {
                // legacy non-UD function "clause"
                fnc = UDep.ROOT
            } else {
                val fncMatch = FULL_DEP_FNC_REGEX.matchEntire(it)
                if (fncMatch != null) {
                    fnc = UDep.parse(fncMatch.groupValues[1])
                    subFnc = fncMatch.groupValues[2].toLowerCase()
                } else {
                    fnc = UDep.parse(it)
                }
            }
        }

        val tok = Token.of(
            id = raw.id(),
            idx = tokenIdx, // sentence based index
            text = text,
            charSpan = CharSpan.withLen(off, text.length),
            origText = origText,
            origCharSpan = CharSpan.withLen(origOff, origText.length),
            deepLemma = raw.string("dLemma"),
            lemma = raw.string("lemma"),
            pos = pos,
            morphTag = raw.string("mTag"),
            feats = readFeats(raw.obj("feats")),
            fnc = fnc,
            subFnc = subFnc
        )
        register(tok)
        return tok
    }

    private fun readTectoToken(raw: JsonObject, tokenIdx: Int): TectoToken {
        val tt = TectoToken.of(
            id = raw.id(),
            idx = tokenIdx, // sentence based index
            fnc = raw.string("fnc") ?: UDep.DEP.toString(),
            lemma = raw.reqString("lemma"),
            feats = readFeats(raw.obj("feats")),
            tokens = readAndResolveIds(raw, "tokenIds"),
            entityMention = null, // will be filled later
            entity = null  // will be filled later
        )
        raw.string("entityMentionId")?.let {
            tectoTokenId2mentionId[tt.id] = it
        }
        register(tt)
        return tt
    }

    private fun <T : Node<T>> createTree(rawTokens: List<JsonObject>, tokens: List<T>): Tree<T>? {
        val tb = TreeBuilder<T>().addNodes(tokens)
        rawTokens.forEach {
            if ("parId" in it) {
                val parent: T = resolveId(it.reqString("parId"))
                val child: T = resolveId(it.id())
                tb.addDependency(childIdx = child.idx, parentIdx = parent.idx)
            }
        }
        return tb.build()
    }

    private fun readSentence(raw: JsonObject, useOrigTextField: Boolean): Sentence {
        val rawTokens: List<JsonObject> = raw.reqArray("tokens")
        val tokens = rawTokens.withIndex().map { (idx, raw) -> readToken(raw, idx, useOrigTextField) }

        val rawTectoTokens: List<JsonObject> = raw.array("tecto") ?: listOf()
        val tectoTokens = rawTectoTokens.withIndex().map { (idx, raw) -> readTectoToken(raw, idx) }

        val tree: Tree<Token>?; val tectoTree: Tree<TectoToken>?
        if (tokens[0].fnc != null) {
            tree = createTree(rawTokens, tokens)
            tectoTree = createTree(rawTectoTokens, tectoTokens)
        } else {
            tree = null
            tectoTree = null
        }

        val sent = Sentence.of(
            id = raw.id(),
            root = tree?.root,
            tokens = tokens,
            tectoRoot = tectoTree?.root,
            tectoTokens = tectoTree?.tokens,
            sentiment = id2sentiment[raw.id()],
            vectors = id2vectors[raw.id()]
        )
        sent.tokens.forEach { it.sentence = sent }
        sent.tectoTokens?.forEach { it.sentence = sent }
        register(sent)
        return sent
    }

    private fun readParagraph(raw: JsonObject): Paragraph {
        val useOrigTextField = version >= SemVer(3, 2, 0)
        val hasCodepointOffs = version >= SemVer(3, 2, 1)

        val text = raw.reqString(if (useOrigTextField) "text" else "corrText")
        val origText = raw.string(if (useOrigTextField) "origText" else "text")
        offMap = if (hasCodepointOffs) OffsetMapping.create(text, origText) else OffsetMapping.IDENTITY

        val para = Paragraph.of(
            id = raw.id(),
            type = raw.reqString("type"),
            text = text,
            origText = origText,
            sentences = raw.array("sentences") { readSentence(it, useOrigTextField) },
            sentiment = id2sentiment[raw.id()],
            vectors = id2vectors[raw.id()]
        )
        para.sentences.forEach { it.paragraph = para }
        register(para)
        return para
    }

    private fun readEntityMention(raw: JsonObject): EntityMention {
        val men = EntityMention.of(
            id = raw.id(),
            mwl = raw.reqString("mwl"),
            text = raw.reqString("text"),
            tokens = readAndResolveIds(raw, "tokenIds") !!, // if entity has a mention, it has to have token support
            derivedFrom = null, // will be filled later
            feats = readFeats(raw.obj("feats")),
            sentiment = id2sentiment[raw.id()],
            vectors = id2vectors[raw.id()]
        )
        raw.string("derivedFromEntityId")?.let {
            val derivedMentions = entityId2derivedMentions.computeIfAbsent(it) { mutableListOf() }
            derivedMentions.add(men)
        }
        register(men)
        return men
    }

    private fun readEntity(raw: JsonObject): Entity {
        val hasGkbProps = version >= SemVer(3, 3, 0)
        val ent = Entity.of(
            id = raw.id(),
            gkbId = raw.string("gkbId"),
            stdForm = raw.reqString("stdForm"),
            type = raw.reqString("type"),
            feats = readFeats(raw.obj("feats")),
            mentions = raw.array("mentions") { readEntityMention(it) },
            gkbProperties = if (hasGkbProps) {
                raw.array("gkbProperties") { readGkbProperty(it) }
            } else {
                emptyList()
            },
            sentiment = id2sentiment[raw.id()],
            vectors = id2vectors[raw.id()]
        )
        ent.mentions.forEach { it.mentionOf = ent }
        register(ent)
        return ent
    }

    private fun readTagMention(raw: JsonObject): TagMention {
        val men = TagMention.of(
            id = raw.id(),
            tokens = readAndResolveIds(raw, "tokenIds") !!, // if tag has a mention, it has to have token support
            feats = readFeats(raw.obj("feats")),
            sentiment = id2sentiment[raw.id()],
            vectors = id2vectors[raw.id()]
        )
        register(men)
        return men
    }

    private fun readTag(raw: JsonObject): Tag {
        val hasGkbProps = version >= SemVer(3, 3, 0)
        val tag = Tag.of(
            id = raw.id(),
            gkbId = raw.string("gkbId"),
            stdForm = raw.reqString("stdForm"),
            type = raw.reqString("type"),
            relevance = raw.reqDouble("relevance"),
            gkbProperties = if (hasGkbProps) {
                    raw.array("gkbProperties") { readGkbProperty(it) }
                } else {
                    emptyList()
                },
            feats = readFeats(raw.obj("feats")),
            mentions = raw.array("mentions") { readTagMention(it) },
            sentiment = id2sentiment[raw.id()],
            vectors = id2vectors[raw.id()]
        )
        tag.mentions.forEach { it.mentionOf = tag }
        register(tag)
        return tag
    }

    private fun readRelationArgument(raw: JsonObject) =
        RelationArgument(
            name = raw.reqString("name"),
            type = raw.reqString("type"),
            entity = readAndResolveId(raw, "entityId")
        )

    private fun readRelationSupport(raw: JsonObject) =
        RelationSupport.of(
            tokens = readAndResolveIds(raw, "tokenIds") !!, // if relation has a support, it has to have token support
            tectoToken = readAndResolveId(raw, "tectoId")
        )

    private fun readRelation(raw: JsonObject): Relation {
        val rel = Relation.of(
            id = raw.id(),
            name = raw.reqString("name"),
            textRepr = raw.reqString("textRepr"),
            type = raw.reqString("type"),
            args = raw.array("args") { readRelationArgument(it) } ?: emptyList(),
            feats = readFeats(raw.obj("feats")),
            support = raw.array("support") { readRelationSupport(it) },
            sentiment = id2sentiment[raw.id()],
            vectors = id2vectors[raw.id()]
        )
        register(rel)
        return rel
    }

    private fun readMetadata(raw: JsonObject): Map<String, Any?>? {
        val useTopLevelMetadata = version <= SemVer(3, 1, 0)

        var metadata = raw.obj("metadata") as Map<String, Any?>?
        val unknownKeys = raw.keys - STD_KEYS
        if (unknownKeys.isNotEmpty()) {
            if (useTopLevelMetadata && metadata == null) {
                metadata = unknownKeys.associateWith { raw[it] }
            } else {
                println("[Warning] unrecognized fields in the analysis: $unknownKeys")
            }
        }
        return metadata
    }

    /** Reads the Analysis object from a JSON object as returned from Geneea G3 API. */
    fun fromJson(rawAnalysis: JsonObject): Analysis {
        checkVersion(rawAnalysis.string("version"))

        // store item sentiment to fill it in when the relevant items are constructed
        rawAnalysis.obj("itemSentiments")?.forEach { (id, obj) ->
            id2sentiment[id] = readSentiment(obj as JsonObject)
        }

        // store item vectors to fill them in when the relevant items are constructed
        rawAnalysis.obj("itemVectors")?.forEach { (id, obj) ->
            @Suppress("UNCHECKED_CAST")
            id2vectors[id] = (obj as JsonArray<JsonObject>).map { readVector(it) }
        }

        // ISO 639-2 for Undetermined language
        val lang = rawAnalysis.obj("language")?.string("detected") ?: "und"

        val paragraphs = rawAnalysis.array("paragraphs") { readParagraph(it) } ?: listOf()
        val entities = rawAnalysis.array("entities") { readEntity(it) } ?: listOf()
        val tags = rawAnalysis.array("tags") { readTag(it) } ?: listOf()
        val relations = rawAnalysis.array("relations") { readRelation(it) } ?: listOf()

        val docSentiment = rawAnalysis.obj("docSentiment")?.let { readSentiment(it) }
        val docVectors = rawAnalysis.array("docVectors") { readVector(it) }

        val analysis = Analysis(
            docId = rawAnalysis.string("id"),
            language = Language(lang),
            paragraphs = paragraphs,
            entities = entities,
            tags = tags,
            relations = relations,
            docSentiment = docSentiment,
            docVectors = docVectors,
            usedChars = rawAnalysis.int("usedChars"),
            metadata = readMetadata(rawAnalysis),
            debugInfo = rawAnalysis["debugInfo"]
        )

        analysis.paragraphs.forEach { it.container = analysis }

        // fill derived-from entities for mentions
        entityId2derivedMentions.forEach { (entityId, derivedMentions) ->
            val entity = resolveId<Entity>(entityId)
            derivedMentions.forEach { it.derivedFrom = entity }
        }

        // fill tecto-token entity mention
        tectoTokenId2mentionId.forEach { (tectoTokenId, mentionId) ->
            val tectoToken = resolveId<TectoToken>(tectoTokenId)
            val mention = resolveId<EntityMention>(mentionId)
            tectoToken.entityMention = mention
            tectoToken.entity = mention.mentionOf
        }

        return analysis
    }

    /** Encapsulates mapping of offsets from code-points to a JVM-char-based. */
    private interface OffsetMapping {
        fun get(cpOff: Int): Int
        fun getOrig(cpOff: Int): Int

        companion object {
            val IDENTITY = object : OffsetMapping {
                override fun get(cpOff: Int) = cpOff
                override fun getOrig(cpOff: Int) = cpOff
            }

            fun create(text: String, origText: String?) : OffsetMapping {
                val convForText = Cp2JvmOffsetConverter(text)
                val convForOrigText = if (origText == null) convForText else Cp2JvmOffsetConverter(origText)

                return object : OffsetMapping {
                    override fun get(cpOff: Int) = convForText.convert(cpOff)
                    override fun getOrig(cpOff: Int) = convForOrigText.convert(cpOff)
                }
            }
        }
    }
}
