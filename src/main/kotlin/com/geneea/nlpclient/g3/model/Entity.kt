package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.common.AbstractHasId
import com.google.common.base.MoreObjects

/** A data class encapsulating an Entity.  */
public class Entity internal constructor(
    /** ID of the entity used to refer to it from other objects  */
    id: String,
    /** Unique identifier of this entity in Geneea knowledge-base  */
    val gkbId: String?,
    /** Standard form of the entity, abstracting from alternative names  */
    val stdForm: String,
    /** Basic type of this entity (e.g. person, location, ...) */
    val type: String,
    /** Actual occurrences of this entity in the text. Empty if not requested/supported. */
    val mentions: List<EntityMention>,
    /** Custom features/properties.  */
    val feats: Map<String, String>,
    /** GKB properties. Since 3.3.0 */
    override val gkbProperties: List<GkbProperty>,
    /** Sentiment of this entity. null if not requested.  */
    override val sentiment: Sentiment?,
    /** Optional vectors for this entity. */
    override val vectors: List<Vector>?
) : AbstractHasId(id), HasGkbProperies, HasSentiment, HasVectors {

    companion object {
        /** Entity factory method, public constructor. */
        public fun of(
            id: String, gkbId: String?,
            stdForm: String, type: String,
            mentions: List<EntityMention>? = null, feats: Map<String, String>? = null,
            gkbProperties: List<GkbProperty>? = null,
            sentiment: Sentiment? = null, vectors: List<Vector>? = null
        ) = Entity(
            id, gkbId, stdForm, type,
            mentions ?: listOf(), feats ?: mapOf(),
            gkbProperties ?: listOf(),
            sentiment, vectors
        )
    }

    public override fun toString(): String = MoreObjects.toStringHelper(this)
        .add("id", id)
        .add("gkbId", gkbId)
        .add("stdForm", stdForm)
        .add("type", type)
        .add("mentions", mentions.ifEmpty { null })
        .add("feats", feats.ifEmpty { null })
        .add("gkbProperties", gkbProperties)
        .add("sentiment", sentiment)
        .add("vectors", vectors)
        .omitNullValues()
        .toString()
}

/** A single occurrence of an entity in the text */
public class EntityMention internal constructor(
    /** ID of the mention used to refer to it from other objects  */
    id: String,
    /** The form of this entity mention, as it occurs in the text.  */
    val text: String,
    /** Lemma of this mention (potentially multiword lemma), i.e. base form of the entity expression.  */
    val mwl: String,
    /** Tokens of this entity mention.  */
    val tokens: TokenSupport,
    /** Custom features/properties. */
    val feats: Map<String, String>,
    /** Entity from which this mention can be derived (e.g. mention `salmon` for entity `fish`), if applicable */
    var derivedFrom: Entity?,
    /** Sentiment of this mention. Note: Not supported yet.  */
    override val sentiment: Sentiment?,
    /** Optional vectors for this mention. */
    override val vectors: List<Vector>?
) : AbstractHasId(id), HasSentiment, HasVectors {

    companion object {
        /** EntityMention factory method, public constructor. */
        public fun of(
            id: String, text: String, mwl: String, tokens: List<Token>,
            feats: Map<String, String>? = null, derivedFrom: Entity? = null,
            sentiment: Sentiment? = null, vectors: List<Vector>? = null
        ) = EntityMention(
            id, text, mwl, TokenSupport.of(tokens),
            feats ?: mapOf(), derivedFrom,
            sentiment, vectors
        )
    }

    /** Entity this mention belongs to  */
    public lateinit var mentionOf: Entity

    /**
     * Sentence containing this entity mention.
     * Entity mention belongs to maximally one sentence; artificial mentions without tokens belong to no sentence.
     */
    public fun sentence(): Sentence = tokens.sentence

    /** Checks whether the entity mention is continuous (most are).  */
    public fun isContinuous(): Boolean = tokens.isContinuous

    /** True iff this entity mention is derived from some other entity (e.g. mention `salmon` for entity `fish`). */
    public fun isDerived(): Boolean = derivedFrom != null

    public override fun toString(): String = MoreObjects.toStringHelper(this)
        .add("id", text)
        .add("mentionOf", mentionOf.id)
        .add("text", text)
        .add("mwl", mwl)
        .add("tokens", tokens)
        .add("feats", feats.ifEmpty { null })
        .add("derivedFrom", derivedFrom?.id)
        .add("sentiment", sentiment)
        .add("vectors", vectors)
        .toString()
}
