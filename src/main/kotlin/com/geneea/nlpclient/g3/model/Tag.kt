package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.common.AbstractHasId
import com.google.common.base.MoreObjects

public class Tag internal constructor(
    /** ID of the tag used to refer to it from other objects  */
    id: String,
    /** Unique identifier of this tag in Geneea knowledge-base. null if not found/linked.  */
    val gkbId: String?,
    /** Standard form of the tag, abstracting from its alternative names  */
    val stdForm: String,
    /** Domain-specific type (e.g. content, theme, iAB, department).  */
    val type: String,
    /** Relevance of the tag relative to the content of the document.  */
    val relevance: Double,
    /** Text segments related to this tag. Empty if not appropriate/requested/supported.  */
    val mentions: List<TagMention>,
    /** Custom features.  */
    val feats: Map<String, String>,
    /** GKB properties. Since 3.3.0 */
    override val gkbProperties: List<GkbProperty>,
    /** Sentiment of this tag. Not supported yet. */
    override val sentiment: Sentiment?,
    /** Optional vectors for this tag. */
    override val vectors: List<Vector>?
) : AbstractHasId(id), HasGkbProperies, HasSentiment, HasVectors {

    companion object {
        /** Type of the tag with the main topic of the document  */
        public const val TYPE_TOPIC = "topic"
        /** Type of the tags with the topic distribution of the document  */
        public const val TYPE_TOPIC_DISTRIBUTION = "topic.distribution"

        /** Tag factory method, public constructor. */
        public fun of(
            id: String, gkbId: String?,
            stdForm: String, type: String, relevance: Double,
            mentions: List<TagMention>? = null, feats: Map<String, String>? = null,
            gkbProperties: List<GkbProperty>? = null,
            sentiment: Sentiment? = null, vectors: List<Vector>? = null
        ) = Tag(
            id, gkbId, stdForm, type, relevance,
            mentions ?: listOf(), feats ?: mapOf(),
            gkbProperties ?: listOf(),
            sentiment, vectors
        )
    }

    public override fun toString(): String = MoreObjects.toStringHelper(this)
        .add("id", id)
        .add("gkbId", gkbId)
        .add("stdForm", stdForm)
        .add("type", type)
        .add("relevance", relevance)
        .add("feats", feats.ifEmpty { null })
        .add("mentions", mentions.ifEmpty { null })
        .add("gkbProperties", gkbProperties)
        .add("sentiment", sentiment)
        .add("vectors", vectors)
        .omitNullValues()
        .toString()
}

/** A single occurrence of a tag in the text */
public class TagMention internal constructor(
    /** ID of the mention used to refer to it from other objects  */
    id: String,
    /** Tokens of this tag mention.  */
    val tokens: TokenSupport,
    /** Custom features/properties. */
    val feats: Map<String, String>,
    /** Sentiment of this mention. Not supported yet.   */
    override val sentiment: Sentiment?,
    /** Optional vectors for this tag mention. */
    override val vectors: List<Vector>?
) : AbstractHasId(id), HasSentiment, HasVectors {

    companion object {
        /** TagMention factory method, public constructor. */
        public fun of(
            id: String, tokens: List<Token>, feats: Map<String, String>? = null,
            sentiment: Sentiment? = null, vectors: List<Vector>? = null
        ) = TagMention(
            id, TokenSupport.of(tokens), feats ?: mapOf(),
            sentiment, vectors
        )
    }

    /** Tag this mention belongs to  */
    public lateinit var mentionOf: Tag

    /**
     * Sentence containing this tag mention.
     * Tag mention belongs to maximally one sentence; artificial mentions without tokens belong to no sentence.
     */
    public fun sentence(): Sentence = tokens.sentence

    /** Checks whether the tag mention is continuous (most are).  */
    public fun isContinuous(): Boolean = tokens.isContinuous

    public override fun toString(): String = MoreObjects.toStringHelper(this)
        .add("id", id)
        .add("mentionOf", mentionOf.id)
        .add("tokens", tokens)
        .add("feats", feats.ifEmpty { null })
        .add("sentiment", sentiment)
        .add("vectors", vectors)
        .omitNullValues()
        .toString()
}
