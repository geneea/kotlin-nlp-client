package com.geneea.nlpclient.g3.model

/** Language of the document.  */
public data class Language(
    /** Language of the document as detected  */
    val detected: String
)
