package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.common.HasId

/** Interface of items that can be assigned a list of vectors. */
public interface HasVectors : HasId {
    /** Vectors of this item. Null when not set. */
    val vectors: List<Vector>?
}

/** Class encapsulating a vector */
public data class Vector(
    /** Name identifying the model of this vector */
    val name: String,
    /** A particular version of the model which produced this vector */
    val version: String,
    /** The vector values */
    val values: DoubleArray
) {

    /** Dimension of this vector. */
    public val dimension: Int = values.size

}
