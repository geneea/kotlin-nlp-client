package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.common.AbstractHasId
import com.google.common.base.MoreObjects

public class Paragraph internal constructor(
    /** ID of the paragraph used to refer to it from other objects  */
    id: String,
    /** title, section heading, lead, body text, etc. For now, it is simply the segment type: title, lead, body */
    val type: String,
    /** the paragraph text, possibly after correction (token offsets link here) */
    val text: String,
    /** the original paragraph text as in the request */
    val origText: String,
    /** the sentences the paragraph consists of  */
    val sentences: List<Sentence>,
    /** Optional sentiment of the paragraph  */
    override val sentiment: Sentiment?,
    /** Optional vectors for this paragraph. */
    override val vectors: List<Vector>?
) : AbstractHasId(id), HasSentiment, HasVectors {

    companion object {
        /** Type of a paragraph representing a title of the whole document. Also used for email subjects.  */
        public const val TYPE_TITLE = "TITLE"
        /** Type of a paragraph representing an abstract (lead or perex) of the whole document  */
        public const val TYPE_ABSTRACT = "ABSTRACT"
        /** Type of a paragraph containing regular text (for now this is used for the whole body of the document)  */
        public const val TYPE_BODY = "BODY"
        /** Type of a paragraph representing a section/chapter heading (not used yet)  */
        public const val TYPE_SECTION_HEADING = "section_heading"

        /** Paragraph factory method, public constructor. */
        public fun of(
            id: String, type: String, text: String,
            origText: String? = null, sentences: List<Sentence>? = null,
            sentiment: Sentiment? = null, vectors: List<Vector>? = null
        ) = Paragraph(
            id, type, text,
            if (origText == null || text == origText) text else origText,
            sentences ?: listOf(), sentiment, vectors
        )
    }

    /** the full analysis object containing this paragraph  */
    public lateinit var container: Analysis

    /** Tokens across all sentences. */
    public fun tokens(): Sequence<Token> = sequence {
        for (s in sentences) {
            yieldAll(s.tokens)
        }
    }

    /** Tecto tokens across all sentences. */
    public fun tectoTokens(): Sequence<TectoToken> = sequence {
        for (s in sentences) {
            s.tectoTokens?.let { yieldAll(it) }
        }
    }

    public override fun toString(): String = MoreObjects.toStringHelper(this)
        .add("id", id)
        .add("type", type)
        .add("text", text)
        .add("origText", origText)
        .add("sentences", sentences.ifEmpty { null })
        .add("sentiment", sentiment)
        .add("vectors", vectors)
        .omitNullValues()
        .toString()
}


