package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.common.HasId

/** Interface of items that can have be assigned a sentiment value. */
public interface HasSentiment : HasId {
    /** Sentiment of this item. Null when not set. */
    val sentiment: Sentiment?
}

/** Class encapsulating sentiment of a document, sentence or relation  */
public data class Sentiment(
    /** Average sentiment  */
    val mean: Double,
    /** Human readable label describing the average sentiment  */
    val label: String,
    /** Average sentiment of positive items  */
    val positive: Double,
    /** Average sentiment of negative items  */
    val negative: Double
)
