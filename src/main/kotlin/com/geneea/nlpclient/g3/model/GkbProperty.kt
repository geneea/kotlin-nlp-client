package com.geneea.nlpclient.g3.model

/** Interface of elements that can be assigned a list of properties. Entities and tags. */
public interface HasGkbProperies {
    /** GKB properties of this item. */
    val gkbProperties: List<GkbProperty>
}

/** Class encapsulating a vector */
public data class GkbProperty(
    /** Key identifying the property. */
    val name: String,
    /** Label describing the property.  */
    val label: String?,
    /** Identifier of GKB item representing property value if there is any.  */
    val valueGkbId: String? = null,
    /** Property value if it is boolean.  */
    val boolValue: Boolean? = null,
    /** Property value if it is float.  */
    val floatValue: Double? = null,
    /** Property value if it is integer.  */
    val intValue: Long? = null,
    /** Property value if it is string or date.  */
    val strValue: String? = null,
) {
    init {
        require(boolValue != null || floatValue != null || intValue != null || strValue != null) {
            "No GKB property value was set."
        }
    }
}
