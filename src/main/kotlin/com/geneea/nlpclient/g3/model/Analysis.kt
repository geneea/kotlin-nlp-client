package com.geneea.nlpclient.g3.model

import com.google.common.base.MoreObjects

/** An object encapsulating the results of NLP analysis. */
public data class Analysis(
    /** Document id  */
    val docId: String?,
    /** Language of the document and analysis.  */
    val language: Language,
    /** The paragraphs within the document. For F2, these are segments.  */
    val paragraphs: List<Paragraph>,
    /** Sentiment of the document.  */
    val docSentiment: Sentiment?,
    /** The entities in the document.  */
    val entities: List<Entity>,
    /** The tags of the document.  */
    val tags: List<Tag>,
    /** The relations in the document.  */
    val relations: List<Relation>,
    /** Optional vectors for the whole document. */
    val docVectors: List<Vector>?,
    /** Characters billed for the analysis.  */
    val usedChars: Int?,
    /** The extra non-NLP type of information related to analysis. */
    val metadata: Map<String, Any?>?,
    /** Debugging information, if any  */
    val debugInfo: Any?
) : HasSentiment, HasVectors {

    public override val id = docId.orEmpty()
    public override val sentiment = docSentiment
    public override val vectors = docVectors

    /** Sentences across all paragraphs. */
    public fun sentences(): Sequence<Sentence> = sequence {
        for (p in paragraphs) {
            yieldAll(p.sentences)
        }
    }

    /** Tokens across all paragraphs. */
    public fun tokens(): Sequence<Token> = sequence {
        for (p in paragraphs) {
            yieldAll(p.tokens())
        }
    }

    /** Tecto tokens across all paragraphs. */
    public fun tectoTokens(): Sequence<TectoToken> = sequence {
        for (p in paragraphs) {
            yieldAll(p.tectoTokens())
        }
    }

    /**
     * Returns the top [n] tags sorted by `relevance` in descending order
     */
    public fun topTags(n: Int = -1): List<Tag> {
        val topTags = tags.sortedByDescending { it.relevance }
        return if (n == -1) topTags else topTags.subList(0, n)
    }

    /**
     * Returns a paragraph with the specified type.
     * Throws IllegalArgumentException if there are more than one such paragraphs, and return null if there are none.
     * This is intended for legacy paragraphs corresponding to title/lead/text segments.
     *
     * For standard types, use these predefined constants:
     * [Paragraph.TYPE_TITLE], [Paragraph.TYPE_ABSTRACT], [Paragraph.TYPE_BODY], [Paragraph.TYPE_SECTION_HEADING],
     * or dedicated methods: [title], [lead], [body].
     */
    public fun getParaByType(paraType: String): Paragraph? {
        val paras = paragraphs.filter { it.type == paraType }
        require(paras.size < 2) { "Multiple paragraphs with the type \"${paraType}\"" }
        return if (paras.size == 1) paras[0] else null
    }

    /** Returns the title paragraph if present, null if not,
     * and throws a IllegalArgumentException if there are multiple title paragraphs. */
    public fun titlePara(): Paragraph? = getParaByType(Paragraph.TYPE_TITLE)

    /** Returns the subject paragraph if present, null if not,
     * and throws a IllegalArgumentException if there are multiple subject paragraphs. */
    public fun subjectPara(): Paragraph? = titlePara()

    /** Returns the abstract paragraph if present, null if not,
     * and throws a IllegalArgumentException if there are multiple abstract paragraphs. */
    public fun abstractPara(): Paragraph? = getParaByType(Paragraph.TYPE_ABSTRACT)

    /** Returns the lead paragraph if present, null if not,
     * and throws a IllegalArgumentException if there are multiple lead paragraphs. */
    public fun leadPara(): Paragraph? = abstractPara()

    /** Returns the perex paragraph if present, null if not,
     * and throws a IllegalArgumentException if there are multiple perex paragraphs. */
    public fun perexPara(): Paragraph? = abstractPara()

    /** Returns the body paragraph if present, null if not,
     * and throws a IllegalArgumentException if there are multiple body paragraphs. */
    public fun bodyPara(): Paragraph? = getParaByType(Paragraph.TYPE_BODY)

    /** Returns the text paragraph if present, null if not,
     * and throws a IllegalArgumentException if there are multiple text paragraphs. */
    public fun textPara(): Paragraph? = bodyPara()

    public override fun toString(): String = MoreObjects.toStringHelper(this)
        .add("docId", docId)
        .add("language", language)
        .add("paragraphs", paragraphs.ifEmpty { null })
        .add("docSentiment", docSentiment)
        .add("entities", entities.ifEmpty { null })
        .add("tags", tags.ifEmpty { null })
        .add("relations", relations.ifEmpty { null })
        .add("docVectors", docVectors)
        .add("usedChars", usedChars)
        .add("metadata", metadata)
        .add("debugInfo", debugInfo)
        .omitNullValues()
        .toString()
}
