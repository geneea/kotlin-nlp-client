package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.common.AbstractHasId
import com.geneea.nlpclient.common.CharSpan
import com.google.common.base.MoreObjects

/** A single sentence with its morphological, syntactical, deep-syntactical and sentimental analysis  */
public class Sentence internal constructor(
    /** ID of the sentence used to refer to it from other objects  */
    id: String,
    /** Token which is the root of the syntactic structure of the sentence  */
    val root: Token?,
    /** All tokens of the sentence ordered by word-order */
    val tokens: List<Token>,
    /** Tecto token which is the root of the tecto structure of the sentence  */
    val tectoRoot: TectoToken?,
    /** All tecto tokens of the sentence; the order has no meaning; null if not available */
    val tectoTokens: List<TectoToken>?,
    /** Optional sentiment of the sentence  */
    override val sentiment: Sentiment?,
    /** Optional vectors for this paragraph. */
    override val vectors: List<Vector>?
) : AbstractHasId(id), HasSentiment, HasVectors {

    companion object {
        /** Type of a paragraph representing a title of the whole document. Also used for email subjects.  */
        public const val TYPE_TITLE = "TITLE"
        /** Type of a paragraph representing an abstract (lead or perex) of the whole document  */
        public const val TYPE_ABSTRACT = "ABSTRACT"
        /** Type of a paragraph containing regular text (for now this is used for the whole body of the document)  */
        public const val TYPE_BODY = "BODY"
        /** Type of a paragraph representing a section/chapter heading (not used yet)  */
        public const val TYPE_SECTION_HEADING = "section_heading"

        /** Sentence factory method, public constructor. */
        public fun of(
            id: String, root: Token?, tokens: List<Token>,
            tectoRoot: TectoToken? = null, tectoTokens: List<TectoToken>? = null,
            sentiment: Sentiment? = null, vectors: List<Vector>? = null
        ) = Sentence(
            id, root, tokens,
            tectoRoot, tectoTokens,
            sentiment, vectors
        )
    }

    /** the paragraph containing this sentence. Null only during G3 Analysis object construction */
    public lateinit var paragraph: Paragraph

    /** text of the sentence, possibly after correction */
    public fun text(): String = charSpan().extractText(paragraph.text)

    /** text span within the paragraph  */
    public fun charSpan(): CharSpan = CharSpan(
        tokens.first().charSpan.start,
        tokens.last().charSpan.end
    )

    /** text of the sentence in the original paragraph */
    public fun origText(): String = origCharSpan().extractText(paragraph.origText)

    /** text span within the original paragraph  */
    public fun origCharSpan(): CharSpan = CharSpan(
        tokens.first().origCharSpan.start,
        tokens.last().origCharSpan.end
    )

    public override fun toString(): String = MoreObjects.toStringHelper(this)
        .add("id", id)
        .add("text", text())
        .add("root", root?.toSimpleString())
        .add("tokens", tokens.toSimpleString(quote = true))
        .add("tectoRoot", tectoRoot?.toSimpleString())
        .add("tectoTokens", tectoTokens?.toSimpleString(quote = true))
        .add("sentiment", sentiment)
        .add("vectors", vectors)
        .omitNullValues()
        .toString()
}
