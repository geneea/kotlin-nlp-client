package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.common.AbstractHasId
import com.geneea.nlpclient.common.toBool
import com.google.common.base.MoreObjects

/** An object encapsulating a relation extracted from text. For example `buy(John, car)`. */
public class Relation internal constructor(
    /** ID of the relation used to refer to it from other objects  */
    id: String,
    /** Human readable representation of the relation, e.g. `eat-not(SUBJ:John, DOBJ:pizza)  */
    val textRepr: String,
    /** Name of the relation , e.g. `eat` for _eat a pizza_ or `good` for _a good pizza_  */
    val name: String,
    /** One of Relation.TYPE_ATTR, Relation.TYPE_RELATION, Relation.TYPE_EXTERNAL  */
    val type: String,
    /** Arguments of the relation (subject, possibly an object).  */
    val args: List<RelationArgument>,
    /** Any features of the relation e.g. modality: can  */
    val support: List<RelationSupport>,
    /** Sentiment of this relation. null if not requested.  */
    val feats: Map<String, String>,
    /** Tecto-tokens of all the mentions of the relations (restricted to its head). Empty if not requested.  */
    override val sentiment: Sentiment?,
    /** Optional vectors for this relation. */
    override val vectors: List<Vector>?
) : AbstractHasId(id), HasSentiment, HasVectors {

    companion object {
        /** Attribute relation (e.g. `good(pizza)` for _good pizza_, _pizza is good_), the attribute is   */
        public const val TYPE_ATTR = "attr"
        /** Verbal relation (e.g. `eat(pizza)` for _eat a pizza._ */
        public const val TYPE_RELATION = "relation"
        /** Relation where at least one argument is outside of the the document (e.g. between `pizza` in the document and
        `food` item in the knowledgebase)  */
        public const val TYPE_EXTERNAL = "external"
        /** key presence signifies it is a negated word, value = True */
        public const val FEAT_NEGATED = "negated"
        /** feature storing info about modality */
        public const val FEAT_MODALITY = "modality"

        /** Relation factory method, public constructor. */
        public fun of(
            id: String, textRepr: String, name: String, type: String, args: List<RelationArgument>,
            support: List<RelationSupport>? = null, feats: Map<String, String>? = null,
            sentiment: Sentiment? = null, vectors: List<Vector>? = null
        ): Relation {
            return Relation(
                id, textRepr, name, type, args,
                support ?: listOf(), feats ?: mapOf(),
                sentiment, vectors
            )
        }
    }

    public fun isNegated(): Boolean = feats[FEAT_NEGATED].toBool()

    public fun modality(): String? = feats[FEAT_MODALITY]

    /** Returns the relation subject argument, if precisely one is present; otherwise returns null */
    public fun subj(): RelationArgument? = args.singleOrNull { it.type.toUpperCase() == "SUBJECT" }

    /** Returns the relation object argument, if precisely one is present; otherwise returns null */
    public fun obj(): RelationArgument? = args.singleOrNull { it.type.toUpperCase() == "OBJECT" }

    public override fun toString(): String = MoreObjects.toStringHelper(this)
        .add("id", textRepr)
        .add("textRepr", textRepr)
        .add("name", name)
        .add("type", type)
        .add("args", args)
        .add("support", support.ifEmpty { null })
        .add("feats", feats.ifEmpty { null })
        .add("sentiment", sentiment)
        .add("vectors", vectors)
        .omitNullValues()
        .toString()
}

/** Object representing an argument (subject/object) of a relations */
public data class RelationArgument(
    /** Name of the argument (e.g. John)  */
    val name: String,
    /** Type of the argument (subject, object)  */
    val type: String,
    /** The entity corresponding to this argument, if any. null if the argument is not an entity.  */
    val entity: Entity?
)

/** Tokens corresponding to a single head (predicate) of a relation  */
public data class RelationSupport internal constructor(
    /** Tokens corresponding to the head of the relation  */
    val tokens: TokenSupport,
    /** Tecto token corresponding to the tokens. null if tecto tokens are not part of the model.   */
    val tectoToken: TectoToken?
) {

    companion object {
        /** RelationSupport factory method, public constructor. */
        public fun of(tokens: List<Token>, tectoToken: TectoToken? = null) =
            RelationSupport(TokenSupport.of(tokens), tectoToken)
    }

    public override fun toString(): String = MoreObjects.toStringHelper(this)
        .add("tokens", tokens)
        .add("tectoToken", tectoToken)
        .omitNullValues()
        .toString()
}
