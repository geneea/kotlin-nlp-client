package com.geneea.nlpclient.g3.model

import com.google.common.base.MoreObjects

/**
 * A tecto token, i.e. a tectogrammatical abstraction of a word (e.g. "did not sleep" are three tokens but a single
 * tecto-token)
 * Tecto tokens have an zero-based index reflecting their position within their sentence.
 */
public class TectoToken internal constructor(
    id: String,
    idx: Int,
    /** Label of the dependency edge.  */
    val fnc: String,
    /** Tecto lemma   */
    val lemma: String,
    /** Grammatical and other features of the tecto token  */
    val feats: Map<String, String>,
    /** Surface token corresponding to this tecto token; not necessarily adjacent; ordered by word-order;
     * Might be null, because the root or dropped phrases have no surface realization
     */
    val tokens: TokenSupport?,
    /** Entity mention associated with this tecto token; null if there is no such entity.  */
    var entityMention: EntityMention?,
    /** Legacy value: Entity associated with this tecto token; null if there is no such entity.
    (Used only by F2, not by G3)  */
    var entity: Entity?
) : Node<TectoToken>(id, idx) {

    companion object {
        /** TectoToken factory method, public constructor. */
        public fun of(
            id: String, idx: Int,
            fnc: String, lemma: String,
            feats: Map<String, String>? = null,
            tokens: List<Token>? = null,
            entityMention: EntityMention? = null, entity: Entity? = null
        ) = TectoToken(
            id, idx, fnc, lemma, feats ?: mapOf(),
            tokens?.let { if (it.isNotEmpty()) TokenSupport(it.sorted(), it.isContinuous()) else null },
            entityMention, entity
        )
    }

    /** Converts the tecto token to a default non-recursive string: index + lemma  */
    public override fun toSimpleString(): String = toStringWith(lemma=true, fnc=false)

    /** Converts the token to a non-recursive string: index + [lemma] + [fnc] */
    public fun toStringWith(lemma: Boolean, fnc: Boolean): String {
        val t = if (lemma) ":${this.lemma}" else ""
        val f = if (fnc) ":${this.fnc}" else ""
        return "${idx}${t}${f}"
    }

    public override fun toString(): String = MoreObjects.toStringHelper(this)
        .add("id", id)
        .add("idx", idx)
        .add("fnc", fnc)
        .add("lemma", lemma)
        .add("feats", feats.ifEmpty { null })
        .add("parent", parent?.toSimpleString())
        .add("children", children.toSimpleString(quote=true))
        .add("tokenSupport", tokens)
        .add("entityMention", entityMention?.id)
        .add("entity", entity?.id)
        .omitNullValues()
        .toString()
}
