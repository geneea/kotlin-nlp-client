package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.common.CharSpan
import com.geneea.nlpclient.common.UDep
import com.geneea.nlpclient.common.UPos
import com.geneea.nlpclient.common.toBool
import com.google.common.base.MoreObjects

/**
 * A token including basic morphological and syntactic information.
 * A token is similar to a word, but includes punctuation.
 * Tokens have an zero-based index reflecting their position within their sentence.
 * The morphological and syntactical features might be null (deepLemma, lemma, morphTag, pos, fnc, parent),
 * or empty (children) if not requested or supported.
 */
public class Token internal constructor(
    id: String,
    idx: Int,
    /** Text of this token, possibly after correction   */
    val text: String,
    /** Character span within the paragraph  */
    val charSpan: CharSpan,
    /** Text of this token in the original paragraph */
    val origText: String,
    /** Character span within the original paragraph */
    val origCharSpan: CharSpan,
    /** Lemma of the token e.g. bezpecny. null if not requested/supported.  */
    val deepLemma: String?,
    /** Simple lemma of the token, e.g. nejnebezpecnejsi (in Cz, includes negation and grade).
    Null if not requested/supported.  */
    val lemma: String?,
    /** Google universal tag. Null if not requested/supported.  */
    val pos: UPos?,
    /** Morphological tag, e.g. AAMS1-...., VBD, ... Null if not requested/supported.  */
    val morphTag: String?,
    /** Universal and custom features  */
    val feats: Map<String, String>,
    /** Label of the dependency edge. Null if not requested/supported.  */
    val fnc: UDep?,
    /** Dependency sub-function. Null if not requested/supported.  */
    val subFnc: String?
) : Node<Token>(id, idx) {

    companion object {
        /** lemma info features, a list of strings */
        public const val FEAT_LEMMA_INFO = "lemmaInfo"
        /** key presence signifies it is a negated word, value = True */
        public const val FEAT_NEGATED = "negated"
        /** key presence signifies it is an unknown word, value = True */
        public const val FEAT_UNKNOWN = "unknown"

        /** Token factory method, public constructor. */
        public fun of(
            id: String, idx: Int,
            text: String, charSpan: CharSpan,
            origText: String? = null, origCharSpan: CharSpan? = null,
            deepLemma: String? = null, lemma: String? = null,
            pos: UPos? = null, morphTag: String? = null,
            feats: Map<String, String>? = null,
            fnc: UDep? = null,
            subFnc: String? = null
        ) = Token(
            id, idx, text, charSpan,
            if (origText == null || text == origText) text else origText,
            if (origCharSpan == null || charSpan == origCharSpan) charSpan else origCharSpan,
            deepLemma, lemma, pos, morphTag, feats ?: mapOf(), fnc, subFnc
        )
    }

    /**
     * Full dependency function in the format `{fnc}:{subFnc}` if the sub-function is present.
     * Otherwise it's the same as `fnc`.
     */
    public val fullFnc: String? by lazy {
        val fncSuffix = subFnc?.let { ":$it" } ?: ""
        fnc?.let { it.toString() + fncSuffix }
    }

    /** True iff the token form contains a negation prefix. */
    public fun isNegated(): Boolean = feats[FEAT_NEGATED].toBool()

    /** True iff the token is unknown to the lemmatizer. The lemma provided is the same as the token itself. */
    public fun isUnknown(): Boolean = feats[FEAT_UNKNOWN].toBool()

    /**
     * Token following or preceding this token within the sentence.
     * @param offset: relative offset. The following tokens have a positive offset,
     *    preceding a negative one. The ext token has offset = 1.
     * @return the token at the relative offset or null if the offset is invalid
     */
    public fun offsetToken(offset: Int): Token? {
        val i = idx + offset
        val tokens = sentence.tokens
        return if (0 <= i && i < tokens.size) tokens[i] else null
    }

    /** The previous token or null if this token is sentence initial.  */
    public fun previous(): Token? = offsetToken(-1)

    /** The next token or null if this token is sentence final. */
    public fun next(): Token? = offsetToken(1)

    /** Converts the token to a default non-recursive string: index + text  */
    public override fun toSimpleString(): String = toStringWith(text=true, pos=false, fnc=false)

    /** Converts the token to a non-recursive string: index + [text] + [pos] + [fnc] */
    public fun toStringWith(text: Boolean, pos: Boolean, fnc: Boolean): String {
        val t = if (text) ":${this.text}" else ""
        val p = if (pos) ":" + (this.pos ?: "_") else ""
        val f = if (fnc) ":" + (this.fnc ?: "_") else ""
        return "${idx}${t}${p}${f}"
    }

    public override fun toString(): String = MoreObjects.toStringHelper(this)
        .add("id", id)
        .add("idx", idx)
        .add("text", text)
        .add("charSpan", charSpan)
        .add("origText", origText)
        .add("origCharSpan", origCharSpan)
        .add("deepLemma", deepLemma)
        .add("lemma", lemma)
        .add("pos", pos)
        .add("morphTag", morphTag)
        .add("feats", feats.ifEmpty { null })
        .add("fnc", fnc)
        .add("subFnc", subFnc)
        .add("parent", parent?.toSimpleString())
        .add("children", children.toSimpleString(quote=true))
        .omitNullValues()
        .toString()
}

/**
 * Tokens within a single sentence; ordered by word-order; non-empty, continuous or discontinuous.
 */
public class TokenSupport internal constructor(
    /** The tokens of this support.  */
    val tokens: List<Token>,
    /** Is this support a continuous sequence of tokens, i.e. a token span?  */
    val isContinuous: Boolean
): List<Token> by tokens {

    companion object {
        /**
         * Creates a TokenSupport object from a list of tokens.
         * @param tokens: non-empty list of tokens (no need for them to be sorted)
         */
        public fun of(tokens: List<Token>): TokenSupport {
            require(tokens.isNotEmpty()) {"TokenSupport cannot be empty."}
            require(tokens.isFromSameSentence()) { "Tokens are not from the same sentence." }
            return TokenSupport(tokens.sorted(), tokens.isContinuous())
        }
    }

    public val sentence: Sentence get() = tokens[0].sentence

    public val paragraph: Paragraph get() = tokens[0].sentence.paragraph

    /** The character span between the first and last token relative to the enclosing paragraph;
    for discontinuous support this includes intervening gaps.  */
    public fun charSpan(): CharSpan = CharSpan(firstCharParaOffset(), lastCharParaOffset())

    /** Offset of the first character of these tokens  within the enclosing paragraph.  */
    public fun firstCharParaOffset(): Int = first().charSpan.start

    /** Offset of the last character of these tokens within the enclosing paragraph.  */
    public fun lastCharParaOffset(): Int = last().charSpan.end

    /**
     * Substring of a full text as denoted by this support (before correction).
     * For discontinuous supports, the result includes the intervening gaps.
     */
    public fun text(): String = charSpan().extractText(sentence.paragraph.text)

    /**
     * Breaks this token support into continuous sub-sequences of tokens.
     */
    public fun spans(): List<TokenSupport> = sequence {
        if (isContinuous) {
            yield(this@TokenSupport)
        } else {
            var start = 0
            var prev = tokens[0]
            for (i in 1 until tokens.size) {
                val cur = tokens[i]
                if (prev.idx + 1 != cur.idx) {
                    yield(TokenSupport(tokens.subList(start, i), true))
                    start = i
                }
                prev = cur
            }
            yield(TokenSupport(tokens.subList(start, tokens.size), true))
        }
    }.toList()

    /** The coverage texts of each of the continuous spans, ordered by word-order. */
    public fun textSpans(): List<String> =  spans().map { it.text() }.toList()

}
