package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.common.isSequential

/** A temporary class used during the construction of a tree of tokens or tecto-tokens */
public data class Tree<N : Node<N>>(
    val root: N,
    val tokens: List<N>
)

public class TreeBuilder<N : Node<N>> {

    private val nodes: MutableList<N> = mutableListOf()
    /* Dependency edges between nodes */
    private val deps: MutableMap<Int, Int> = mutableMapOf()

    /**
     * Record a single token as a node of the tree.
     * @param node: token to add. Its index must be correct, parent and children fields are ignored.
     * @return the builder to allow chained calls
     */
    public fun addNode(node: N): TreeBuilder<N> {
        nodes.add(node)
        return this
    }

    /**
     * Record a collection of tokens as nodes of the tree.
     * @param nodes: tokens to add. Their index must be correct, parent and children fields are ignored.
     * @return the builder to allow chained calls
     */
    public fun addNodes(nodes: Iterable<N>): TreeBuilder<N> {
        this.nodes.addAll(nodes)
        return this
    }

    /**
     * Record a dependency edge. The tokens connected by the edge might be added later.
     * @param childIdx: index of the child token (note: tokens are indexed within their sentences)
     * @param parentIdx: index of the parent token (note: tokens are indexed within their sentences)
     * @return the builder to allow chained calls
     */
    public fun addDependency(childIdx: Int, parentIdx: Int): TreeBuilder<N> {
        require(childIdx >= 0) { "Negative node index $childIdx." }
        require(parentIdx >= 0) { "Negative node index $parentIdx." }
        require(childIdx != parentIdx) { "Dependency edge cannot be reflexive." }
        require(!deps.containsKey(childIdx) || deps[childIdx] == parentIdx)
        { "Node $childIdx has multiple parents: [${nodes[parentIdx].id}, ${nodes[deps[childIdx]!!].id}]." }

        deps[childIdx] = parentIdx
        return this
    }

    /** All nodes are hanged to the first one. */
    public fun addDummyDependencies() {
        require(deps.isEmpty()) { "Dummy dependencies cannot be added when other dependencies have been specified." }
        if (nodes.isEmpty()) return

        nodes.sort()
        for (n in nodes.subList(1, nodes.size)) {
            addDependency(n.idx, 0)
        }
    }

    private fun fillParents() {
        val maxIdx = nodes.size - 1

        for ((c, p) in deps) {
            require(c <= maxIdx) { "The child of the dependency edge $c -> $p is out of range (max=$maxIdx)." }
            require(p <= maxIdx) { "The parent of the dependency edge $c -> $p is out of range (max=$maxIdx)." }

            nodes[c].parent = nodes[p]
        }
    }

    private fun findRoot(): N {
        val roots = nodes.filter { it.isRoot() }

        check(roots.isNotEmpty()) { "No root." }
        check(roots.size == 1) { "Multiple roots: ${roots.map { it.id }}." }

        return roots[0]
    }

    private fun fillChildren() {
        for (n in nodes) {
            n.children = nodes.filter { it.parent == n }.toList()
        }
    }

    /** Builds a tree based on the current state. Should not be called more than once. */
    public fun build(): Tree<N>? {
        if (nodes.isEmpty()) return null

        // Creates an ordered dependency tree based on the contents this builder.
        nodes.sort()
        check(nodes[0].idx == 0 && nodes.map { it.idx }.isSequential() ) { "Indexes are not sequential." }

        fillParents()
        val root = findRoot() // exactly one root check; addDependency checks for multiple parents => tree
        fillChildren()

        return Tree(root, nodes)
    }
}
