package example.quickStart

import com.geneea.nlpclient.g3.client.AnalysisType
import com.geneea.nlpclient.g3.client.Client
import com.geneea.nlpclient.g3.client.Request
import com.geneea.nlpclient.g3.io.writeToJson

val USER_KEY: String? = null    // fill your API key (or assign it to the GENEEA_API_KEY env variable)

val TEXTS = listOf(
    "I was in Paris last week. The Eiffel tower is great.",
    "You should try Prague, too.",
    "But I always love to go back to London. Going there next Tuesday."
)

fun main() {
    val client = Client.create(userKey = USER_KEY)
    val builder = Request.Builder().analyses(AnalysisType.ENTITIES)

    TEXTS.withIndex().forEach { (idx, text) ->
        val req = builder.build(id = "$idx", text = text)
        val analysis = client.analyze(req)
        println(writeToJson(analysis).toJsonString())
    }
}

