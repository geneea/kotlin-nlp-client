package example.quickStartMedia

import com.geneea.nlpclient.g3.client.AnalysisType
import com.geneea.nlpclient.g3.io.writeToJson
import com.geneea.nlpclient.g3.media.MediaClient
import com.geneea.nlpclient.g3.media.NlpRequest

val API_KEY: String? = null    // fill your API key (or assign it to the GENEEA_API_KEY env variable)
val CUSTOMER_ID: String? = null    // fill your Customer ID (or assign it to the GENEEA_CUSTOMER_ID env variable)

val TEXTS = listOf(
    "I was in Paris last week. The Eiffel tower is great.",
    "You should try Prague, too.",
    "But I always love to go back to London. Going there next Tuesday."
)

fun main() {
    val client = MediaClient.create(apiKey = API_KEY, customerId = CUSTOMER_ID)
    val builder = NlpRequest.Builder().analyses(AnalysisType.ENTITIES)

    TEXTS.withIndex().forEach { (idx, text) ->
        val req = builder.build(id = "$idx", text = text)
        val analysis = client.analyze(req)
        println(writeToJson(analysis).toJsonString())
    }
}
