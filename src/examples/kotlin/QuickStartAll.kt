package example.quickStartAll

import com.geneea.nlpclient.g3.client.AnalysisType
import com.geneea.nlpclient.g3.client.Client
import com.geneea.nlpclient.g3.client.Request
import java.time.LocalDate

val USER_KEY: String? = null    // fill your API key (or assign it to the GENEEA_API_KEY env variable)

val TEXTS = listOf(
    "I was in Paris last week. The Eiffel tower is great.",
    "You should try Prague, too.",
    "But I always love to go back to London. Going there next Tuesday."
)

fun main() {
    val client = Client.create(userKey = USER_KEY)
    val builder = Request.Builder()
        .analyses(AnalysisType.ALL)
        .referenceDate(LocalDate.of(2001, 12, 13))
        .returnMentions(true)
        .returnItemSentiment(true)

    TEXTS.withIndex().forEach { (idx, text) ->
        val req = builder.build(id = "$idx", text = text)

        val analysis = client.analyze(req)

        println(text); println()

        println("--- Entities ---")
        analysis.entities.forEach { entity ->
            println("\t${entity.type}: ${entity.stdForm}")

            entity.mentions.forEach { mention ->
                val prevToken = mention.tokens.first().previous()
                val nextToken = mention.tokens.last().next()
                val snippet = listOfNotNull(
                    prevToken?.text, "[${mention.text}]", nextToken?.text
                ).joinToString(" ")

                println("\t\t${mention.mwl}: $snippet")
            }
        }
        println()

        println("--- Tags ---")
        analysis.tags.forEach { tag ->
            println("\t${tag.type}: ${tag.stdForm}")

            tag.mentions.forEach { mention ->
                val prevToken = mention.tokens.first().previous()
                val nextToken = mention.tokens.last().next()
                val snippet = listOfNotNull(
                    prevToken?.text, "[${mention.tokens.text()}]", nextToken?.text
                ).joinToString(" ")

                println("\t\t${snippet}")
            }
        }
        println()

        println("--- Relations ---")
        analysis.relations.forEach { relation ->
            val argsStr = relation.args.joinToString(", ") { "${it.type}: ${it.name}" }
            val modalityStr = relation.modality() ?: ""
            val negatedStr = if (relation.isNegated()) "-not" else ""

            println("\t${relation.type}: ${modalityStr}${relation.name}${negatedStr}(${argsStr})")

        }
        println()

        println("--- Document Sentiment ---")
        println("\t${analysis.docSentiment}")
        println()

        println("------------------")
        println()
    }
}

