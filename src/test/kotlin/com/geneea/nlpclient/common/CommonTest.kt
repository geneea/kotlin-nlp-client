package com.geneea.nlpclient.common

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class TestCommon {

    @Test
    fun `test if integers in a list are sequential`() {
        assertTrue(listOf(1, 2, 3, 4, 5).isSequential())
        assertTrue(listOf(-1, 0, 1).isSequential())
        assertTrue(listOf(0).isSequential())
        assertTrue(listOf(5).isSequential())
        assertTrue(listOf<Int>().isSequential())

        assertFalse(listOf(1, 2, 3, 5).isSequential())
        assertFalse(listOf(1, 1).isSequential())
        assertFalse(listOf(1, 0).isSequential())
    }

    @Test
    fun `conversion from string values to boolean`() {
        assertTrue(" TRUE ".toBool())
        assertTrue("true".toBool())
        assertTrue("1".toBool())

        assertFalse(null.toBool())
        assertFalse("Yes".toBool())
        assertFalse("anything".toBool())
    }
}
