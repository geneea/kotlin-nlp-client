package com.geneea.nlpclient.common

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class OffsetConverterTest {
    @Test
    fun `Codepoints offsets to JVM-char offsets`() {
        val text = "V 🐦🎾 obchodě chybí nová alba 🤣."
        val spans = listOf(
            // start codepoint, end codepoint (exl), text
            Triple(0, 1, "V"),
            Triple(2, 4, "🐦🎾"),
            Triple(5, 12, "obchodě"),
            Triple(13, 18, "chybí"),
            Triple(19, 23, "nová"),
            Triple(24, 28, "alba"),
            Triple(29, 30, "🤣"),
            Triple(30, 31, ".")
        )

        val converter = Cp2JvmOffsetConverter(text)

        for (span in spans) {
            val len = span.third.length
            val charOffset = converter.convert(span.first)
            val charSpan = CharSpan.withLen(charOffset, len)

            assertEquals(span.third, charSpan.extractText(text))
        }
    }

    @Test
    fun `offset conversion from codepoints to JVM and back`() {
        val text = "V 🐦🎾 obchodě chybí nová alba 🤣."
        val origSpans = listOf(
            // start codepoint, end codepoint (exl), text
            Triple(0, 1, "V"),
            Triple(2, 4, "🐦🎾"),
            Triple(5, 12, "obchodě"),
            Triple(13, 18, "chybí"),
            Triple(19, 23, "nová"),
            Triple(24, 28, "alba"),
            Triple(29, 30, "🤣"),
            Triple(30, 31, ".")
        )

        val converterToJvm = Cp2JvmOffsetConverter(text)
        val converterToCp = Jvm2CpOffsetConverter(text)

        val charSpans = origSpans
            .map {
                val charOffset = converterToJvm.convert(it.first)
                CharSpan.withLen(charOffset, it.third.length)
            }

        val cpSpans = charSpans
            .map {
                val cpStartOffset = converterToCp.convert(it.start)
                val cpEndOffset = converterToCp.convert(it.end)
                Pair(cpStartOffset, cpEndOffset)
            }

        for ( (orig, back) in origSpans zip cpSpans) {
            assertEquals(orig.first, back.first)
            assertEquals(orig.second, back.second)
        }
    }

}
