package com.geneea.nlpclient.common

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class TestCharSpan {

    @Test
    fun `test for of()`() {
        assertEquals(CharSpan(0, 8), CharSpan.withLen(0, 8))
        assertEquals(CharSpan(4, 8), CharSpan.withLen(4, 4))
        assertEquals(CharSpan(10, 11), CharSpan.withLen(10, 1))
    }

    @Test
    fun `test for length`() {
        assertEquals(CharSpan(4, 8).length, 4)
        assertEquals(CharSpan(10, 11).length, 1)
    }

    @Test
    fun `check arguments for of()`() {
        assertThrows<IllegalArgumentException> { CharSpan.of(4, 4) }
        assertThrows<IllegalArgumentException> { CharSpan.of(4, 3) }
        assertThrows<IllegalArgumentException> { CharSpan.of(4, -1) }
        assertThrows<IllegalArgumentException> { CharSpan.of(-1, 3) }
        assertThrows<IllegalArgumentException> { CharSpan.of(-1, -1) }
    }

    @Test
    fun `check arguments for withLen()`() {
        assertThrows<IllegalArgumentException> { CharSpan.withLen(-1, 2) }
        assertThrows<IllegalArgumentException> { CharSpan.withLen(1, -2) }
        assertThrows<IllegalArgumentException> { CharSpan.withLen(-1, -2) }
        assertThrows<IllegalArgumentException> { CharSpan.withLen(4, 0) }
    }

    @Test
    fun `test for extractText()`() {
        assertEquals(CharSpan(2, 5).extractText("abcdefgh"), "cde")
        assertEquals(CharSpan(2, 3).extractText("abcdefgh"), "c")
        assertEquals(CharSpan(0, 3).extractText("abcdefgh"), "abc")
        assertEquals(CharSpan(0, 1).extractText("abcdefgh"), "a")
        assertEquals(CharSpan(2, 4).extractText("abcd"), "cd")
        assertEquals(CharSpan(3, 4).extractText("abcd"), "d")
    }

    @Test
    fun `span is outside of the text`() {
        assertThrows<IllegalArgumentException> { CharSpan(3, 6).extractText("abcd") }
    }
}
