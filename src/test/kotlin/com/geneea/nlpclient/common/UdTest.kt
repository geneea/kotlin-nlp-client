package com.geneea.nlpclient.common

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestUd {

    @Test
    fun `string to UPos enum conversion`() {
        assertEquals(UPos.ADJ, UPos.parse("ADJ"))
        assertEquals(UPos.ADJ, UPos.parse("adj"))
        assertEquals(UPos.ADJ, UPos.parse("Adj"))
        assertEquals(UPos.X, UPos.parse("unknown"))
        assertEquals(UPos.X, UPos.parse(""))
    }

    @Test
    fun `string to UDep enum conversion`() {
        assertEquals(UDep.DOBJ, UDep.parse("DOBJ"))
        assertEquals(UDep.DOBJ, UDep.parse("dobj"))
        assertEquals(UDep.DOBJ, UDep.parse("DObj"))
        assertEquals(UDep.DEP, UDep.parse("unknown"))
    }
}
