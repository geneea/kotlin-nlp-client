package com.geneea.nlpclient.common

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class IdTest {

    @Test
    fun `test for ids()`() {
        val x = object : HasId {
            override val id = "x"
        }
        val y = object : HasId {
            override val id = "y"
        }

        assertEquals(listOf<String>(), listOf<HasId>().ids())
        assertEquals(listOf("x", "y"), listOf(x, y).ids())
        assertEquals(listOf("x", "y", "x"), listOf(x, y, x).ids())
    }

}
