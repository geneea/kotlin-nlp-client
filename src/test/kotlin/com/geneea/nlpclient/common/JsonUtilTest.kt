package com.geneea.nlpclient.common

import com.beust.klaxon.JsonObject
import com.geneea.nlpclient.createJsonDiff
import com.geneea.nlpclient.toPrettyString
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.ClassCastException

class JsonBuilderTest {

    private fun assertJsonEquals(expectedJson: String, actual: JsonObject) {
        val diff = createJsonDiff(expectedJson, actual)
        assertTrue(diff.isEmpty())
        {"different JSON - number of diffs ${diff.size}:\n${diff.toPrettyString()}"}
    }

    @Test
    fun `create JSON from pairs`() {
        assertJsonEquals(
            """ {"a": "v", "b": 1,"d": [3, 5, 7], "f": {"g": 7}} """,
            json(
                "a" to "v",
                "b" to 1,
                "c" to (null as String?),  // null is omitted
                "d" to listOf(3, 5, 7),
                "e" to listOf<Int>(),  // empty list is omitted
                "f" to json( "g" to 7),
                "h" to json()       // empty json object (map) is omitted
            )
        )
    }

    @Test
    fun `create JSON with embedded objects`() {
        assertJsonEquals(
            """ {"a": {"b": 7}, "d": {"e": {"f": 11, "g": [12, 13]}, "h": [{"i": 20}, {"i": 30}]}} """,
            json(
                "a" to json( "b" to 7),
                "c" to json(),
                "d" to json(
                    "e" to json(
                        "f" to 11,
                        "g" to listOf(12, 13)
                    ),
                    "h" to listOf(
                        json( "i" to 20),
                        json("i" to 30)
                    )
                )
            )
        )
    }

    @Test
    fun `reqDouble accepts Number`() {
        val numbers = json(
            "int" to 7,
            "double" to 7.0,
            "long" to 7L,
            "string" to "7.0"
        )
        assertEquals(7.0, numbers.reqDouble("int"))
        assertEquals(7.0, numbers.reqDouble("double"))
        assertEquals(7.0, numbers.reqDouble("long"))
        assertThrows<ClassCastException> {numbers.reqDouble("string")}
    }
}
