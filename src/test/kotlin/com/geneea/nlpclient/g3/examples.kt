package com.geneea.nlpclient.g3

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.json.JsonMapper
import java.nio.file.Files
import java.nio.file.Paths
import java.util.stream.Stream

private val jsonMapper = JsonMapper()

private object Examples

private fun readJson(exampleFilename: String): JsonNode {
    val path = "/examples/$exampleFilename"
    Examples::class.java.getResourceAsStream(path)!!.use {
        return jsonMapper.readTree(it)
    }
}

fun allAnalysisExamples(): Stream<Pair<String, JsonNode>> {
    val dirUri = Examples::class.java.getResource("/examples")!!.toURI()
    return Files.list(Paths.get(dirUri))
            .filter { it.fileName.toString().startsWith("example") }
            .map { it.fileName.toString() to readJson(it.fileName.toString()) }
}

fun exampleAnalysis() = readJson("example.json")

fun exampleAnalysisFull() = readJson("example_FULL.json")

fun exampleSentiment() = readJson("example_SENTIMENT.json")

fun exampleVector() = readJson("example_DOCVECTOR.json")

fun exampleVectors() = readJson("example_DOCVECTORS.json")

fun exampleAnalysisV311() = readJson("readonly_example_G3v311_TOKENS.json")

fun exampleAnalysisV320() = readJson("readonly_example_G3v320_TOKENS.json")

fun exampleAnalysisV321() = readJson("readonly_example_G3v321_TOKENS.json")

fun exampleAnalysisBugfixSDK37() = readJson("example_bugfix.SDK-37.json")

fun exampleAnalysisBugfixSDK39() = readJson("readonly_example_bugfix.SDK-39.json")

fun exampleAnalysisBugfixSDK44() = readJson("example_bugfix.SDK-44.json")

fun exampleAnalysisBugfixSDK46() = readJson("readonly_example_bugfix.SDK-46.json")

fun exampleTagsWithProps() = readJson("example_TAGS_PROPS.json")

fun exampleEntitiesWithProps() = readJson("example_ENTITIES_PROPS.json")

fun exampleRequest() = readJson("request.json")

fun exampleNlpRequest() = readJson("nlp_request.json")
