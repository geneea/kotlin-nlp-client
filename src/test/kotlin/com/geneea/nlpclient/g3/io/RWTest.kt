package com.geneea.nlpclient.g3.io

import com.geneea.nlpclient.Diff
import com.geneea.nlpclient.asJsonObject
import com.geneea.nlpclient.common.UDep
import com.geneea.nlpclient.createJsonDiff
import com.geneea.nlpclient.g3.*
import com.geneea.nlpclient.g3.model.GkbProperty
import com.geneea.nlpclient.g3.model.RelationArgument
import com.geneea.nlpclient.g3.model.Token
import com.geneea.nlpclient.toPrettyString
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class RWTest {

    @Test
    fun `read write should produce the original input for example files`() {
        allAnalysisExamples().forEach { (filename, input) ->
            val analysis = readFromJson(input.asJsonObject())
            val output = writeToJson(analysis)

            val diff = createJsonDiff(input, output).filterNot { isSpurious(it) }.toList()
            assertTrue(diff.isEmpty())
            {"[$filename] different JSON - number of diffs ${diff.size}:\n${diff.toPrettyString()}"}
        }
    }

    @Test
    fun `test read tokens`() {
        val obj = readFromJson(exampleAnalysisFull().asJsonObject())

        val t0 = obj.paragraphs[0].sentences[0].tokens[0] // Angela

        assertEquals(t0.id, "w0")
        assertEquals("Angela", t0.text)
        assertEquals(0, t0.charSpan.start)
        assertEquals(6, t0.charSpan.end)
        assertEquals("Angela", t0.origText)
        assertEquals(0, t0.origCharSpan.start)
        assertEquals(6, t0.origCharSpan.end)
        assertEquals("Angela", t0.deepLemma)
        assertEquals(null, t0.lemma) // missing
        assertEquals("NNP", t0.morphTag)
        assertEquals(UDep.COMPOUND, t0.fnc)
        assertEquals(mapOf("negated" to "false"), t0.feats)

        assertEquals("Merkel", t0.parent!!.text)
        assertEquals(listOf<Token>(), t0.children)
        assertEquals(null, t0.previous())
        assertEquals("Merkel", t0.next()!!.text)
    }

    @Test
    fun `test read tecto`() {
        val obj = readFromJson(exampleAnalysisFull().asJsonObject())

        val d0 = obj.paragraphs[0].sentences[0].tectoTokens!![0] // root
        val d1 = obj.paragraphs[0].sentences[0].tectoTokens!![1] // Angela Merkel
        val w0 = obj.paragraphs[0].sentences[0].tokens[0] // Angela
        val w1 = obj.paragraphs[0].sentences[0].tokens[1] // Merkel

        assertEquals("d1", d1.id)
        assertEquals("Angela Merkel", d1.lemma)
        assertEquals(d0, d1.parent)
        assertEquals("clause", d1.fnc)
        assertEquals(listOf(w0, w1), d1.tokens!!.tokens)
    }

    @Test
    fun `test orig vs corr text`() {
        fun test(toks: List<Token>) {
            assertEquals("chybí", toks[3].text)
            assertEquals(15, toks[3].charSpan.start)
            assertEquals(20, toks[3].charSpan.end)
            assertEquals("chybý", toks[3].origText)
            assertEquals(15, toks[3].origCharSpan.start)
            assertEquals(20, toks[3].origCharSpan.end)
            assertEquals("nové", toks[4].text)
            assertEquals(21, toks[4].charSpan.start)
            assertEquals(25, toks[4].charSpan.end)
            assertEquals("nové", toks[4].origText)
            assertEquals(21, toks[4].origCharSpan.start)
            assertEquals(25, toks[4].origCharSpan.end)
            assertEquals("alba", toks[5].text)
            assertEquals(26, toks[5].charSpan.start)
            assertEquals(30, toks[5].charSpan.end)
            assertEquals("albumy", toks[5].origText)
            assertEquals(26, toks[5].origCharSpan.start)
            assertEquals(32, toks[5].origCharSpan.end)
            assertEquals("🤣", toks[6].text)
            assertEquals(31, toks[6].charSpan.start)
            assertEquals(33, toks[6].charSpan.end)
            assertEquals("🤣", toks[6].origText)
            assertEquals(33, toks[6].origCharSpan.start)
            assertEquals(35, toks[6].origCharSpan.end)
            assertEquals(".", toks[7].text)
            assertEquals(33, toks[7].charSpan.start)
            assertEquals(34, toks[7].charSpan.end)
            assertEquals(".", toks[7].origText)
            assertEquals(35, toks[7].origCharSpan.start)
            assertEquals(36, toks[7].origCharSpan.end)
        }

        val withOrig = exampleAnalysisV321().asJsonObject()
        var obj = readFromJson(withOrig)
        test(obj.paragraphs[0].sentences[0].tokens)

        val withCorr = exampleAnalysisV311().asJsonObject()
        obj = readFromJson(withCorr)
        test(obj.paragraphs[0].sentences[0].tokens)

        assertEquals(
            withOrig.toJsonString(canonical = true),
            writeToJson(obj).toJsonString(canonical = true)
        )
    }

    @Test
    fun `test codepoint offsets`() {
        fun test(text: String, toks: List<Token>) {
            assertEquals("🐦🎾", toks[1].text)
            assertEquals(2, toks[1].charSpan.start)
            assertEquals(6, toks[1].charSpan.end)
            assertEquals("🐦🎾", toks[1].charSpan.extractText(text))
            assertEquals("obchodě", toks[2].text)
            assertEquals(7, toks[2].charSpan.start)
            assertEquals(14, toks[2].charSpan.end)
            assertEquals("obchodě", toks[2].charSpan.extractText(text))
            assertEquals("chybí", toks[3].text)
            assertEquals(15, toks[3].charSpan.start)
            assertEquals(20, toks[3].charSpan.end)
            assertEquals("chybí", toks[3].charSpan.extractText(text))
            assertEquals("nové", toks[4].text)
            assertEquals(21, toks[4].charSpan.start)
            assertEquals(25, toks[4].charSpan.end)
            assertEquals("nové", toks[4].charSpan.extractText(text))
            assertEquals("alba", toks[5].text)
            assertEquals(26, toks[5].charSpan.start)
            assertEquals(30, toks[5].charSpan.end)
            assertEquals("alba", toks[5].charSpan.extractText(text))
            assertEquals("🤣", toks[6].text)
            assertEquals(31, toks[6].charSpan.start)
            assertEquals(33, toks[6].charSpan.end)
            assertEquals("🤣", toks[6].charSpan.extractText(text))
            assertEquals(".", toks[7].text)
            assertEquals(33, toks[7].charSpan.start)
            assertEquals(34, toks[7].charSpan.end)
            assertEquals(".", toks[7].charSpan.extractText(text))
        }

        val withCodepoint = exampleAnalysisV321().asJsonObject()
        var obj = readFromJson(withCodepoint)
        test(obj.paragraphs[0].text, obj.paragraphs[0].sentences[0].tokens)

        val notCodepoint = exampleAnalysisV320().asJsonObject()
        obj = readFromJson(notCodepoint)
        test(obj.paragraphs[0].text, obj.paragraphs[0].sentences[0].tokens)

        assertEquals(
            withCodepoint.toJsonString(canonical = true),
            writeToJson(obj).toJsonString(canonical = true)
        )
    }

    @Test
    fun `SDK-37 one entity mention at multiple tecto tokens`() {
        val js = exampleAnalysisBugfixSDK37().asJsonObject()
        val obj = readFromJson(js)

        val men = obj.entities[1].mentions[0]
        val tts = obj.paragraphs[0].sentences[0].tectoTokens!!

        assertEquals("Letuška.cz", men.text)
        assertEquals("Letuška", tts[12].tokens?.text())
        assertEquals(men, tts[12].entityMention)
        assertEquals(".", tts[13].tokens?.text())
        assertEquals(men, tts[13].entityMention)
        assertEquals("cz", tts[14].tokens?.text())
        assertEquals(men, tts[14].entityMention)

        assertEquals(
            js.toJsonString(canonical = true),
            writeToJson(obj).toJsonString(canonical = true)
        )
    }

    @Test
    fun `Read sentiment with integer value`() {
        val js = exampleSentiment().asJsonObject()
        val obj = readFromJson(js)

        val sentiment = obj.docSentiment!!

        assertEquals("positive", sentiment.label)
        assertEquals(0.1, sentiment.mean)
        assertEquals(0.0, sentiment.negative)
        assertEquals(0.1, sentiment.positive)
    }

    @Test
    fun `SDK-39 empty relation args`() {
        val js = exampleAnalysisBugfixSDK39().asJsonObject()
        val obj = readFromJson(js)

        val rel = obj.relations[1]

        assertTrue(rel.args.isEmpty())
    }

    @Test
    fun `SDK-44 dependency sub-function`() {
        val js = exampleAnalysisBugfixSDK44().asJsonObject()
        val obj = readFromJson(js)

        val toks = obj.tokens().toList()

        assertEquals(UDep.NMOD, toks[0].fnc)
        assertEquals("poss", toks[0].subFnc)
        assertEquals("nmod:poss", toks[0].fullFnc)

        assertEquals(UDep.NSUBJ, toks[1].fnc)
        assertEquals("pass", toks[1].subFnc)
        assertEquals("nsubj:pass", toks[1].fullFnc)

        assertEquals(UDep.AUX, toks[2].fnc)
        assertNull(toks[2].subFnc)
        assertEquals("aux", toks[2].fullFnc)

        assertEquals(
            js.toJsonString(canonical = true),
            writeToJson(obj).toJsonString(canonical = true)
        )
    }

    @Test
    fun `SDK-46 missing relation args`() {
        val js = exampleAnalysisBugfixSDK46().asJsonObject()
        val analysis = readFromJson(js)

        assertEquals(emptyList<RelationArgument>(), analysis.relations[0].args)
    }

    @Test
    fun `test read docVector`() {
        val obj = readFromJson(exampleVector().asJsonObject())

        assertEquals(obj.docVectors?.size, 1)

        val vector = obj.docVectors?.get(0)
        assertEquals(vector?.name, "vector5")
        assertEquals(vector?.version, "3.14")
        assertArrayEquals(vector?.values, arrayOf(0.01, -0.02, 0.03, -0.04, 0.05).toDoubleArray())
    }

    @Test
    fun `test read docVectors`() {
        val obj = readFromJson(exampleVectors().asJsonObject())

        assertEquals(obj.docVectors?.size, 2)

        val vector1 = obj.docVectors?.get(0)
        assertEquals(vector1?.name, "vector5")
        assertEquals(vector1?.version, "3.14")
        assertArrayEquals(vector1?.values, arrayOf(0.01, -0.02, 0.03, -0.04, 0.05).toDoubleArray())

        val vector2 = obj.docVectors?.get(1)
        assertEquals(vector2?.name, "vector3")
        assertEquals(vector2?.version, "version 3.15")
        assertArrayEquals(vector2?.values, arrayOf(0.01, -0.02, 0.03).toDoubleArray())
    }

    @Test
    fun `tags with properties`() {
        val js = exampleTagsWithProps().asJsonObject()
        val analysis = readFromJson(js)

        val expectedProps = listOf(
            GkbProperty("description", "popis", strValue = "německá politička"),
            GkbProperty("region", "Kraj", "G1588", strValue = "Louisiana"),
            GkbProperty("country", "Stát", "G30", strValue = "USA"),
            GkbProperty("population", "Počet obyvatel", intValue = 383997)
        )
        assertEquals(emptyList<GkbProperty>(), analysis.tags[0].gkbProperties)
        assertEquals(expectedProps, analysis.tags[1].gkbProperties)

        assertEquals(
            js.toJsonString(canonical = true),
            writeToJson(analysis).toJsonString(canonical = true)
        )
    }

    @Test
    fun `entity with properties`() {
        val js = exampleEntitiesWithProps().asJsonObject()
        val analysis = readFromJson(js)

        val expectedProps = listOf(
            GkbProperty("description", "popis", strValue = "město v USA"),
            GkbProperty("region", "Kraj", "G1588", strValue = "Louisiana"),
            GkbProperty("country", "Stát", "G30", strValue = "USA"),
            GkbProperty("population", "Počet obyvatel", intValue = 383997)
        )
        assertEquals(emptyList<GkbProperty>(), analysis.entities[0].gkbProperties)
        assertEquals(expectedProps, analysis.entities[1].gkbProperties)

        assertEquals(
            js.toJsonString(canonical = true),
            writeToJson(analysis).toJsonString(canonical = true)
        )
    }

    /**
     * Predicate true for irrelevant G3 json differences -
     * 1) empty list and null are considered the same for tags, tag.mentions, entity, entity.mentions, paragraphs
     * 2) clause is replaced by root for token syntactic functions
     * 3) tokenIds can be missing when empty
     */
    private fun isSpurious(diffItem: Diff) =
        (
            diffItem.op == "add" &&
            diffItem.path.matches("""/(paragraphs|relations|(tags|entities(/[0-9]+/mentions)?))""".toRegex()) &&
            (diffItem.value == null || diffItem.value.toList().isEmpty())
        ) || (
            // UD fnc: "clause" > root
            diffItem.op == "replace" &&
            diffItem.path.matches("""/paragraphs/\d+/sentences/\d+/tokens/\d+/fnc""".toRegex()) &&
            (diffItem.orig!!.asText() == "clause" && diffItem.value!!.asText() == "root")
        ) || (
            // empty token ids
            diffItem.op == "remove" &&
            diffItem.path.matches("""/.*/tokenIds""".toRegex()) &&
            (diffItem.orig!!.isEmpty)
        )
}
