package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.common.CharSpan
import com.geneea.nlpclient.common.UDep
import com.geneea.nlpclient.common.UPos
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

fun pretendToken(idx: Int, text: String, charStart: Int = 0) = Token.of(
    id = "t${idx}",
    idx = idx,
    text = text,
    charSpan = CharSpan.withLen(charStart, text.length),
    deepLemma = "DL${text}",
    lemma = "L${text}",
    pos = UPos.X,
    fnc = UDep.DEP,
    morphTag = "X"
)

fun buildTestTree(tokens: List<Token>, deps: List<Pair<Int, Int>>): Tree<Token> {
    val builder = TreeBuilder<Token>()
    for (token in tokens) {
        builder.addNode(token)
    }
    for (dep in deps) {
        builder.addDependency(dep.first, dep.second)
    }

    val tree =  builder.build() !!
    val sentence = Sentence.of(
        id = "id0",
        root = tree.root,
        tokens = tree.tokens
    )
    tree.tokens.forEach { it.sentence = sentence }
    return tree
}

/** Tokens get valid indexes and space-separated spans */
fun buildTestStrTree(tokenStrs: List<String>, deps: List<Pair<Int, Int>>): Tree<Token> {
    var charOffset = 0

    val tokens = arrayListOf<Token>()
    for ((idx, text) in tokenStrs.withIndex()) {
        tokens.add(pretendToken(idx, text, charOffset))
        charOffset += text.length + 1
    }

    return buildTestTree(tokens, deps)
}

/** For testing indexes, token spans are not valid */
fun buildIdxTree(idxs: Iterable<Int>, deps: List<Pair<Int, Int>>): Tree<Token> =
    buildTestTree(
        idxs.map { pretendToken(it, it.toString()) }.toList(),
        deps
    )


class TestTreeBuilder {

    @Test
    fun `build a tree with 6 tokens dependent on the last one`() {
        // 6 tokens all depend on the last one
        val tree = buildIdxTree(0..5, listOf(0 to 5, 1 to 5, 2 to 5, 3 to 5, 4 to 5))
        for (i in 0..5) {
            assertEquals(tree.tokens[i].idx, i)
        }

        assertEquals(tree.root, tree.tokens[5])
        assertEquals(tree.root.parent, null)

        for (token in tree.tokens.subList(0, 5)) {
            assertEquals(token.parent, tree.root)
            assertEquals(token.children, listOf<Token>())
        }
	}

    @Test
    fun `build a tree with 6 tokens each dependent on the previous one`() {
        // 6 tokens all depend on the previous one
        val tree = buildIdxTree(0..5, listOf(1 to 0, 2 to 1, 3 to 2, 4 to 3, 5 to 4))
        for (i in 0..5) {
            assertEquals(tree.tokens[i].idx, i)
        }

        assertEquals(tree.root, tree.tokens[0])
        assertEquals(tree.root.parent, null)

        assertEquals(tree.tokens[0].parent, null)
        assertEquals(tree.tokens[1].parent, tree.tokens[0])
        assertEquals(tree.tokens[2].parent, tree.tokens[1])
        assertEquals(tree.tokens[3].parent, tree.tokens[2])
        assertEquals(tree.tokens[4].parent, tree.tokens[3])
        assertEquals(tree.tokens[5].parent, tree.tokens[4])

        assertEquals(tree.tokens[0].children, listOf(tree.tokens[1]))
        assertEquals(tree.tokens[1].children, listOf(tree.tokens[2]))
        assertEquals(tree.tokens[2].children, listOf(tree.tokens[3]))
        assertEquals(tree.tokens[3].children, listOf(tree.tokens[4]))
        assertEquals(tree.tokens[4].children, listOf(tree.tokens[5]))
        assertEquals(tree.tokens[5].children, listOf<Token>())
	}

    @Test
    fun `build a tree with dummy dependencies`() {
        val builder = TreeBuilder<Token>()
        for (idx in 0..5) {
            builder.addNode(pretendToken(idx, idx.toString()))
        }
        builder.addDummyDependencies()

        val tree = builder.build() !!
        for (i in 0..5) {
            assertEquals(tree.tokens[i].idx, i)
        }
        assertEquals(tree.root, tree.tokens[0])
        assertEquals(tree.root.parent, null)

        assertEquals(tree.tokens[0].parent, null)
        assertEquals(tree.tokens[1].parent, tree.tokens[0])
        assertEquals(tree.tokens[2].parent, tree.tokens[0])
        assertEquals(tree.tokens[3].parent, tree.tokens[0])
        assertEquals(tree.tokens[4].parent, tree.tokens[0])
        assertEquals(tree.tokens[5].parent, tree.tokens[0])

        assertEquals(tree.tokens[0].children, listOf(
            tree.tokens[1], tree.tokens[2], tree.tokens[3], tree.tokens[4], tree.tokens[5]
        ))
        assertEquals(tree.tokens[1].children, listOf<Token>())
        assertEquals(tree.tokens[2].children, listOf<Token>())
        assertEquals(tree.tokens[3].children, listOf<Token>())
        assertEquals(tree.tokens[4].children, listOf<Token>())
        assertEquals(tree.tokens[5].children, listOf<Token>())
	}

    @Test
    fun `build nonprojective tree`() {
        // ( 0 and 1 depend on 5 and are fronted; 4 is the root)
        val tree = buildIdxTree(0..5, listOf(0 to 1, 1 to 5, 2 to 3, 3 to 4, 5 to 4))
        for (i in 0..5) {
            assertEquals(tree.tokens[i].idx, i)
        }
	}

    @Test
    fun `error - nonconsecutive token indexes`() {
        // token idx=3 missing
        val ex = assertThrows<IllegalStateException> {
            buildIdxTree(listOf(0, 1, 2, 4, 5), listOf(0 to 5, 1 to 5, 2 to 5, 4 to 5))
        }
        assertEquals("Indexes are not sequential.", ex.message)
	}

    @Test
    fun `error - first token with non-zero index`() {
        // token idx=0 missing
        val ex = assertThrows<IllegalStateException> {
            buildIdxTree(1..5, listOf(1 to 5, 2 to 5, 3 to 5, 4 to 5))
        }
        assertEquals("Indexes are not sequential.", ex.message)
	}

    @Test
    fun `error - tree with multiple roots`() {
        val ex = assertThrows<IllegalStateException> {
            buildIdxTree(0..5, listOf(0 to 2, 1 to 2, 3 to 5, 4 to 5))
        }
        assertEquals("Multiple roots: [t2, t5].", ex.message)
	}

    @Test
    fun `error - circular edges`() {
        val ex = assertThrows<IllegalArgumentException> {
            buildIdxTree(0..5, listOf(0 to 1, 1 to 2, 2 to 3, 3 to 4, 4 to 5, 4 to 2))
        }
        assertEquals("Node 4 has multiple parents: [t2, t5].", ex.message)
	}
}
