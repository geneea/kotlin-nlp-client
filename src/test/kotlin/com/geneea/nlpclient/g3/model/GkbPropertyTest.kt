package com.geneea.nlpclient.g3.model

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class GkbPropertyTest {

    @Test
    fun `GKB property has to have one value`() {
        val ex = Assertions.assertThrows(java.lang.IllegalArgumentException::class.java) {
            GkbProperty(name = "foo", label = "bar")
        }
        Assertions.assertEquals("No GKB property value was set.", ex.message)
        Assertions.assertNotNull(GkbProperty(name = "foo", label = "bar", strValue = "acme"))
    }
}