package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.asJsonObject
import com.geneea.nlpclient.common.CharSpan
import com.geneea.nlpclient.g3.exampleAnalysis
import com.geneea.nlpclient.g3.io.readFromJson
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class TestTokenSupport {

    private val tree = buildTestStrTree(
        listOf("a", "b", "c", "d", "e", "f"),
        listOf(0 to 1, 1 to 2, 2 to 3, 3 to 4, 4 to 5)
    )

    @Test
    fun test_a() {
        // [a] b c d e f
        //  0 12345678901
        val sup = TokenSupport.of(listOf(tree.tokens[0]))
        assertEquals(sup.size, 1)
        assertTrue(sup.isContinuous)

        val spans = sup.spans()
        assertEquals(listOf(sup), spans.toList())

        assertEquals(sup.first(), tree.tokens[0])
        assertEquals(sup.last(), tree.tokens[0])

        assertEquals(sup.charSpan(), CharSpan(0, 1))
        assertEquals(sup.firstCharParaOffset(), 0)
        assertEquals(sup.lastCharParaOffset(), 1)
	}

    @Test
    fun test_bcd() {
        // a [b c d] e f
        // 01 23456 78901
        val sup = TokenSupport.of(listOf(tree.tokens[1], tree.tokens[2], tree.tokens[3]))
        assertEquals(sup.size, 3)
        assertTrue(sup.isContinuous)

        val spans = sup.spans()
        assertEquals(listOf(sup), spans.toList())

        assertEquals(sup.first(), tree.tokens[1])
        assertEquals(sup.last(), tree.tokens[3])

        assertEquals(sup.charSpan(), CharSpan(2, 7))
        assertEquals(sup.firstCharParaOffset(), 2)
        assertEquals(sup.lastCharParaOffset(), 7)
	}

    @Test
    fun test_b_de() {
        // a [b] c [d e] f
        // 01 2 345 678 901
        val sup = TokenSupport.of(listOf(tree.tokens[1], tree.tokens[3], tree.tokens[4]))
        assertEquals(sup.size, 3)
        assertFalse(sup.isContinuous)

        val spans = sup.spans().toList()
        assertEquals(2, spans.size)
        assertEquals(listOf(tree.tokens[1]), spans[0].tokens)
        assertEquals(listOf(tree.tokens[3], tree.tokens[4]), spans[1].tokens)

        assertEquals(sup.first(), tree.tokens[1])
        assertEquals(sup.last(), tree.tokens[4])

        assertEquals(sup.charSpan(), CharSpan(2, 9))
        assertEquals(sup.firstCharParaOffset(), 2)
        assertEquals(sup.lastCharParaOffset(), 9)
	}

    @Test
    fun test_of_check() {
        assertThrows<IllegalArgumentException> {
            TokenSupport.of(listOf())
        }

        val obj = readFromJson(exampleAnalysis().asJsonObject())
        // para[1] "Angela Merkel left Germany. She move to New Orleans to learn jazz. That"s amazing."

        // token support across two sentences
        assertThrows<IllegalArgumentException> {
            TokenSupport.of(listOf(
                obj.paragraphs[1].sentences[0].tokens.last(),
                obj.paragraphs[1].sentences[1].tokens.first()
            ))
        }
        assertThrows<IllegalArgumentException> {
            TokenSupport.of(listOf(
                obj.paragraphs[0].sentences[0].tokens.last(),
                obj.paragraphs[1].sentences[0].tokens.first()
            ))
        }
	}

    @Test
    fun test_of_text() {
        val obj = readFromJson(exampleAnalysis().asJsonObject())
        // para[1] "Angela Merkel left Germany. She move to New Orleans to learn jazz. That"s amazing."

        val tokens = obj.paragraphs[1].sentences[0].tokens

        var sup = TokenSupport.of(tokens.subList(0,1))
        assertEquals(sup.text(), "Angela")
        assertEquals(sup.textSpans(), listOf("Angela"))

        sup = TokenSupport.of(tokens.subList(2, 3))
        assertEquals(sup.text(), "left")
        assertEquals(sup.textSpans(), listOf("left"))

        sup = TokenSupport.of(tokens.subList(0, 3))
        assertEquals(sup.text(), "Angela Merkel left")
        assertEquals(sup.textSpans(), listOf("Angela Merkel left"))

        sup = TokenSupport.of(tokens.subList(0, 1) + tokens.subList(2, 3))
        assertEquals(sup.text(), "Angela Merkel left")
        assertEquals(sup.textSpans(), listOf("Angela", "left"))
	}
}
