package com.geneea.nlpclient.g3.model

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class TokenTest {

    @Test
    fun test_inOrder() {
        var tree = buildIdxTree(0..5, listOf(0 to 1, 1 to 2, 2 to 3, 3 to 4, 4 to 5))
        assertEquals(listOf("0", "1", "2", "3", "4", "5"), tree.root.inOrder().map{it.text}.toList())

        tree = buildIdxTree(0..5, listOf(0 to 4, 1 to 4, 2 to 4, 3 to 4, 4 to 5))
        assertEquals(listOf("0", "1", "2", "3", "4", "5"), tree.root.inOrder().map{it.text}.toList())

        tree = buildTestStrTree(
            listOf("0", "1", "2", "3", "4", "5"),
            listOf(0 to 2, 2 to 3, 3 to 4, 4 to 5, 1 to 5)
        )
        assertEquals(listOf("1", "0", "2", "3", "4", "5"), tree.root.inOrder().map{it.text}.toList())
    }

    @Test
    fun test_filteredInOrder() {
        var tree = buildIdxTree(0..5, listOf(0 to 1, 1 to 2, 2 to 3, 3 to 4, 4 to 5))
        assertEquals(
            listOf("1", "2", "3", "4", "5"),
            tree.root.filteredInOrder {it.text == "1"} .map{it.text}.toList()
        )

        assertEquals(
            listOf("2", "3", "4", "5"),
            tree.root.filteredInOrder(false) {it.text == "1"} .map{it.text}.toList()
        )

        tree = buildIdxTree(
            0..5,
            listOf(0 to 4, 1 to 4, 2 to 4, 3 to 4, 4 to 5)
        )
        assertEquals(
            listOf("0", "1", "2", "3", "4", "5"),
            tree.root.filteredInOrder {it.text == "1"} .map{it.text}.toList()
        )
        assertEquals(
            listOf("4", "5"),
            tree.root.filteredInOrder {it.text == "4"} .map{it.text}.toList()
        )

        assertEquals(
                listOf("5"),
                tree.root.filteredInOrder(false) {it.text == "4"} .map{it.text}.toList()
        )
    }

    @Test
    fun test_preOrder() {
        var tree = buildIdxTree(0..5, listOf(0 to 1, 1 to 2, 2 to 3, 3 to 4, 4 to 5))
        assertEquals(
            listOf("5", "4", "3", "2", "1", "0"),
            tree.root.preOrder().map{it.text}.toList()
        )

        tree = buildIdxTree(0..5, listOf(0 to 4, 1 to 4, 2 to 4, 3 to 4, 4 to 5))
        assertEquals(
            listOf("5", "4", "0", "1", "2", "3"),
            tree.root.preOrder().map{it.text}.toList()
        )

        // b splitting [ac]
        tree = buildIdxTree(0..5, listOf(0 to 2, 2 to 3, 3 to 4, 4 to 5, 1 to 5))
        assertEquals(
            listOf("5", "1", "4", "3", "2", "0"),
            tree.root.preOrder().map{it.text}.toList()
        )
    }

    @Test
    fun test_filteredPreOrder() {
        var tree = buildIdxTree(0..5, listOf(0 to 1, 1 to 2, 2 to 3, 3 to 4, 4 to 5))
        assertEquals(
            listOf("5", "4", "3", "2", "1"),
            tree.root.filteredPreOrder{ it.text == "1" }.map{it.text}.toList()
        )

        assertEquals(
            listOf("5", "4", "3", "2"),
            tree.root.filteredPreOrder(false){ it.text == "1" }.map{it.text}.toList()
        )

        tree = buildIdxTree(0..5, listOf(0 to 4, 1 to 4, 2 to 4, 3 to 4, 4 to 5))
        assertEquals(
            listOf("5", "4", "0", "1", "2", "3"),
            tree.root.filteredPreOrder{ it.text == "1" }.map{it.text}.toList()
        )

        assertEquals(
            listOf("5", "4"),
            tree.root.filteredPreOrder{ it.text == "4" }.map{it.text}.toList()
        )

        assertEquals(
            listOf("5"),
            tree.root.filteredPreOrder(false){ it.text == "4" }.map{it.text}.toList()
        )
    }
}

