package com.geneea.nlpclient.g3.model

import com.geneea.nlpclient.asJsonObject
import com.geneea.nlpclient.g3.exampleAnalysis
import com.geneea.nlpclient.g3.io.readFromJson
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class TestAnalysis {

    private val obj = readFromJson(exampleAnalysis().asJsonObject())

    @Test
    fun test_token_offsetToken() {
        assertEquals(obj.paragraphs[0].sentences[0].tokens[0], obj.paragraphs[0].sentences[0].tokens[0].offsetToken(0))

        // non-existing offset yields null
        assertEquals(null, obj.paragraphs[0].sentences[0].tokens[0].offsetToken(-1))
        assertEquals(null, obj.paragraphs[0].sentences[0].tokens[0].offsetToken(-10))
        assertEquals(null, obj.paragraphs[0].sentences[0].tokens[1].offsetToken(-2))
        assertEquals(null, obj.paragraphs[0].sentences[0].tokens[1].offsetToken(-10))

        assertEquals(obj.paragraphs[0].sentences[0].tokens[0], obj.paragraphs[0].sentences[0].tokens[1].offsetToken(-1))
        assertEquals(obj.paragraphs[0].sentences[0].tokens[1], obj.paragraphs[0].sentences[0].tokens[2].offsetToken(-1))
        assertEquals(obj.paragraphs[0].sentences[0].tokens[0], obj.paragraphs[0].sentences[0].tokens[2].offsetToken(-2))

        // non-existing offset yields null
        assertEquals(null, obj.paragraphs[0].sentences[0].tokens.asReversed()[0].offsetToken(1))
        assertEquals(null, obj.paragraphs[0].sentences[0].tokens.asReversed()[0].offsetToken(10))
        assertEquals(null, obj.paragraphs[0].sentences[0].tokens.asReversed()[1].offsetToken(2))
        assertEquals(null, obj.paragraphs[0].sentences[0].tokens.asReversed()[1].offsetToken(10))

        assertEquals(obj.paragraphs[0].sentences[0].tokens[1], obj.paragraphs[0].sentences[0].tokens[0].offsetToken(1))
        assertEquals(obj.paragraphs[0].sentences[0].tokens[2], obj.paragraphs[0].sentences[0].tokens[0].offsetToken(2))
        assertEquals(obj.paragraphs[0].sentences[0].tokens[2], obj.paragraphs[0].sentences[0].tokens[1].offsetToken(1))
    }

    @Test
    fun test_token_charSpan_extractText() {
        val para0 = obj.paragraphs[0]
        val para1 = obj.paragraphs[1]

        assertEquals("Angela", para0.sentences[0].tokens[0].charSpan.extractText(para0.text))
        assertEquals("New", para0.sentences[0].tokens[3].charSpan.extractText(para0.text))
        assertEquals("Angela", para1.sentences[0].tokens[0].charSpan.extractText(para1.text))
        assertEquals("Germany", para1.sentences[0].tokens[3].charSpan.extractText(para1.text))
        assertEquals("That", para1.sentences[2].tokens[0].charSpan.extractText(para1.text))
        assertEquals("amazing", para1.sentences[2].tokens[2].charSpan.extractText(para1.text))
    }

    @Test
    fun test_getParaByType() {
        assertEquals(obj.paragraphs[0], obj.getParaByType(Paragraph.TYPE_TITLE))
        assertEquals(obj.paragraphs[1], obj.getParaByType(Paragraph.TYPE_BODY))
    }
}
