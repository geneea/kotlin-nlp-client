package com.geneea.nlpclient.g3.media

import com.geneea.nlpclient.asJsonObject
import com.geneea.nlpclient.createJsonDiff
import com.geneea.nlpclient.g3.client.AnalysisType
import com.geneea.nlpclient.g3.client.LanguageCode
import com.geneea.nlpclient.g3.exampleNlpRequest
import com.geneea.nlpclient.toPrettyString
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeParseException

class TestNlpRequest {

    private val reqJson = exampleNlpRequest()

    @Test
    fun `simple request build`() {
        var diff = createJsonDiff(
            """{"text": "Unit Test"}""",
            NlpRequest.Builder().build(text="Unit Test").toJson()
        )
        assertTrue(diff.isEmpty())
        { "different JSON:\n${diff.toPrettyString()}" }

        diff = createJsonDiff(
            """{"title": "", "text": ""}""",
            NlpRequest.Builder().build(title="", text="").toJson()
        )
        assertTrue(diff.isEmpty())
        { "different JSON:\n${diff.toPrettyString()}" }
    }

    @Test
    fun `more advanced request build`() {
        val bldr = NlpRequest.Builder()
            .analyses(AnalysisType.ENTITIES, AnalysisType.SENTIMENT)
            .language(LanguageCode.EN)
            .langDetectPrior("de,en")
            .presentationLanguage(LanguageCode.CS)
            .returnMentions(true)
            .returnItemSentiment(true)
            .returnGkbProperties(true)
            .metadata(mapOf("foo" to "bar"))
            .customConfig(mapOf("baz" to 1.0))

        val builtReq = bldr.build(
            title="Angela Merkel in New Orleans",
            lead="Angela Merkel left Germany.",
            text="She move to New Orleans to learn jazz. That\'s amazing.",
            domicile = "New Orleans, USA",
            categories = listOf(Category(Taxonomy.MediaTopic, "lifestyle and leisure (10000000)"))
        )

        val diff = createJsonDiff(reqJson, builtReq.toJson())
        assertTrue(diff.isEmpty())
        {"different JSON - number of diffs ${diff.size}:\n${diff.toPrettyString()}"}
    }

    @Test
    fun `read and write request`() {
        val req = NlpRequest.fromJson(reqJson.asJsonObject())
        val output = req.toJson()

        val diff = createJsonDiff(reqJson, output)
        assertTrue(diff.isEmpty())
        {"different JSON - number of diffs ${diff.size}:\n${diff.toPrettyString()}"}
    }

    @Test
    fun `ref date`() {
        val bldr = NlpRequest.Builder()

        assertEquals("2019-05-12", bldr.referenceDate( "2019-05-12").referenceDate)
        assertEquals("2019-05-02", bldr.referenceDate( "2019-5-2").referenceDate)
        assertEquals("2019-05-12", bldr.referenceDate(LocalDate.of(2019, 5, 12)).referenceDate)
        assertEquals("NOW", bldr.referenceDate( "now").referenceDate)

        assertThrows<DateTimeParseException> { bldr.referenceDate("5.12.2019") }
        assertThrows<DateTimeParseException> { bldr.referenceDate("2019-15-02") }
    }

    @Test
    fun `ref date time`() {
        val bldr = NlpRequest.Builder()

        assertEquals("2019-05-12T01:02:03", bldr.referenceDate( "2019-05-12T01:02:03").referenceDate)
        assertEquals("2019-05-12T01:02:03Z", bldr.referenceDate( "2019-05-12T01:02:03Z").referenceDate)
        assertEquals(
            "2019-05-12T01:02:03+01:00", bldr.referenceDate( "2019-05-12T01:02:03+01:00").referenceDate)
        val localDateTime = LocalDateTime.of(2019, 5, 12, 1, 2, 3)
        assertEquals("2019-05-12T01:02:03", bldr.referenceDate(localDateTime).referenceDate)
        assertEquals("NOW", bldr.referenceDate( "now").referenceDate)

        assertThrows<DateTimeParseException> { bldr.referenceDate("5.12.2019 01:02:03") }
        assertThrows<DateTimeParseException> { bldr.referenceDate("2019-15-12T01:02:03") }
        assertThrows<DateTimeParseException> { bldr.referenceDate("2019-05-12T1:2:3") }
        assertThrows<DateTimeParseException> { bldr.referenceDate("2019-15-12T25:02:03") }
    }
}

class TestNlpRequestTaxonomy {
    @Test
    fun test_parse() {
        assertEquals(Taxonomy.MediaTopic, Taxonomy.parse("MeDiAtOpIc"))
        assertEquals(Taxonomy.SubjectCode, Taxonomy.parse("SUBJECTCODE"))
        assertEquals(Taxonomy.Custom, Taxonomy.parse("Custom"))
        assertThrows<IllegalArgumentException> { Taxonomy.parse("media-topic") }
    }
}