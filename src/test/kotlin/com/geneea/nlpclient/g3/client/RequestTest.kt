package com.geneea.nlpclient.g3.client

import com.fasterxml.jackson.databind.JsonNode
import com.geneea.nlpclient.asJsonObject
import com.geneea.nlpclient.createJsonDiff
import com.geneea.nlpclient.g3.exampleRequest
import com.geneea.nlpclient.g3.io.readFromJson
import com.geneea.nlpclient.g3.io.writeToJson
import com.geneea.nlpclient.toPrettyString
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.LocalDate
import java.time.format.DateTimeParseException

class TestRequest {

    private val reqJson = exampleRequest()

    @Test
    fun `simple request build`() {
        var diff = createJsonDiff(
            """{"text": "Unit Test"}""",
            Request.Builder().build(text="Unit Test").toJson()
        )
        assertTrue(diff.isEmpty())
        { "different JSON:\n${diff.toPrettyString()}" }

        diff = createJsonDiff(
            """{"title": "", "text": ""}""",
            Request.Builder().build(title="", text="").toJson()
        )
        assertTrue(diff.isEmpty())
        { "different JSON:\n${diff.toPrettyString()}" }
    }

    @Test
    fun `more advanced request build`() {
        val bldr = Request.Builder()
            .analyses(AnalysisType.ENTITIES, AnalysisType.SENTIMENT)
            .domain("news")
            .language(LanguageCode.EN)
            .textType(TextType.CLEAN)
            .diacritization(Diacritization.YES)
            .returnMentions(true)
            .returnItemSentiment(true)
            .customConfig(mapOf("custom_key" to listOf("custom value")))

        val builtReq = bldr.build(
            title="Angela Merkel in New Orleans",
            text="Angela Merkel left Germany. She move to New Orleans to learn jazz. That\'s amazing."
        )

        val diff = createJsonDiff(reqJson, builtReq.toJson())
        assertTrue(diff.isEmpty())
        {"different JSON - number of diffs ${diff.size}:\n${diff.toPrettyString()}"}
    }

    @Test
    fun `read and write request`() {
        val req = Request.fromJson(reqJson.asJsonObject())
        val output = req.toJson()

        val diff = createJsonDiff(reqJson, output)
        assertTrue(diff.isEmpty())
        {"different JSON - number of diffs ${diff.size}:\n${diff.toPrettyString()}"}
    }

    @Test
    fun `ref date`() {
        val bldr = Request.Builder()

        assertEquals("2019-05-12", bldr.referenceDate( "2019-05-12").referenceDate)
        assertEquals("2019-05-02", bldr.referenceDate( "2019-5-2").referenceDate)
        assertEquals("2019-05-12", bldr.referenceDate(LocalDate.of(2019, 5, 12)).referenceDate)
        assertEquals("NOW", bldr.referenceDate( "now").referenceDate)

        assertThrows<DateTimeParseException> { bldr.referenceDate("5.12.2019") }
        assertThrows<DateTimeParseException> { bldr.referenceDate("2019-15-02") }
    }
}
