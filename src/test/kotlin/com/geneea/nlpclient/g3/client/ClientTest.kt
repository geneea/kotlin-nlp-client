package com.geneea.nlpclient.g3.client

import okhttp3.OkHttpClient
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

internal class ClientTest {

    @Test
    fun `additional setup block is applied on OkHttpClient`() {
        val mockSetupBlock: (OkHttpClient.Builder) -> Unit = mock()

        Client.create(additionalSetupBlock = mockSetupBlock)

        verify(mockSetupBlock).invoke(any())
    }
}