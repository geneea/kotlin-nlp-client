package com.geneea.nlpclient

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.json.JsonMapper
import com.flipkart.zjsonpatch.JsonDiff

private val jsonMapper = JsonMapper()
private val jsonParser = Parser.default()

/** Info about a single json difference */
data class Diff(
    /** add, remove, replace */
    val op: String,
    val path: String,
    val orig: JsonNode?,
    val value: JsonNode?
)

fun Iterable<Diff>.toPrettyString() =
    this.joinToString(separator = "\n")
    { "${it.op} @ ${it.path} : ${it.orig} > ${it.value}" }


fun createJsonDiff(source: JsonNode, target: JsonNode): List<Diff> =
    JsonDiff.asJson(source, target).map {
        val op = it.get("op").asText()
        val path = it.get("path").asText()
        val value = it.get("value")
        val orig = if (op == "remove" || op == "replace") source.at(path) else null

        Diff(op, path, orig, value)
    }.toList()

fun createJsonDiff(source: String, target: String) =
    createJsonDiff(jsonMapper.readTree(source), jsonMapper.readTree(target))

fun createJsonDiff(source: String, target: JsonNode) =
    createJsonDiff(jsonMapper.readTree(source), target)

fun createJsonDiff(source: JsonNode, target: String) =
    createJsonDiff(source, jsonMapper.readTree(target))

fun createJsonDiff(source: JsonObject, target: JsonObject) =
    createJsonDiff(source.toJsonString(), target.toJsonString())

fun createJsonDiff(source: JsonObject, target: JsonNode) =
    createJsonDiff(source.toJsonString(), target)

fun createJsonDiff(source: JsonNode, target: JsonObject) =
    createJsonDiff(source, target.toJsonString())

fun createJsonDiff(source: JsonObject, target: String) =
    createJsonDiff(source.toJsonString(), target)

fun createJsonDiff(source: String, target: JsonObject) =
    createJsonDiff(source, target.toJsonString())


fun JsonNode.asJsonObject() =
    jsonParser.parse(this.toString().reader()) as JsonObject

